<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="net.tanesha.recaptcha.ReCaptcha" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaFactory" %>
<%@ page import="com.iacademy.LottoGo.beans.Bet" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>LottoGo | Payment</title>
	<%@include file="styles/importCss.html" %>
</head>
<body>    
	<%
	String payment = (String)session.getAttribute("payment");
	String membership = (String)session.getAttribute("membership");
	if(payment != null && membership != null){
	%>
	
	<%session.removeAttribute("payment"); %>
    <!--BLOCK FOR NAVBAR-->
    <div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<a href="home.jsp" class="navbar-brand pull-left"><img src="images/Logo.png" id="logoHeader"></a>

			<button class="navbar-toggle" data-toggle="collapse" data-target=".navHeaderCollapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>

			<div class = "collapse navbar-collapse navHeaderCollapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="home.jsp"><span class="glyphicon glyphicon-home"></span> Home</a></li>
					<li><a href="rules.jsp"><span class="glyphicon glyphicon-bullhorn"></span> Rules</a></li>
					<li><a href="pcso.jsp"><span class="glyphicon glyphicon-check"></span> PCSO Accreditation</a></li>
					<li><a href="about.jsp"><span class="glyphicon glyphicon-info-sign"></span> About</a></li>
					<%@include file="styles/navExtension.jsp" %>
				</ul>
			</div>
		</div>
	</div>
	
	<!--SignUp Form-->
	<div class="jumbotron specialJumbo firstObject">
		<div class="container-fluid">
			<h1 class="inLineHeading"><span class="glyphicon glyphicon-ok"></span>All Done!</h1>
		</div><br>
		
		<div class="container well well-sm">
			<div class="form-horizontal wrapper-signup" role="form">
				<center><br><br>
				<div class="alert alert-success" role="alert"><b>Transaction Complete. Thank you for using LottoGo!</b></div>
				<div></div>
				</center>
			</div>
		</div><br><br><br><br><br><br><br><br><br><br><br><br>
		
	</div>	
    
	<!-- Scripts -->
    <%@include file="styles/scripts.html" %>
    
	<%}else{
		response.sendRedirect("error.jsp");
	} %>
</body>
</html>