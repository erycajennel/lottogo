<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@include file="styles/importCss.html" %>
	<title>LottoGo | ERROR</title>	
	
</head>
<body>
    <image src="images/brownBlueBg.png" id="HomeBG"/>
    
    <!--BLOCK FOR NAVBAR-->
    <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <a href="." class="navbar-brand pull-left"><img src="images/Logo.png" id="logoHeader"></a>
            </div>
        </div><br>
    
    <div class="col-sm-12 firstObject">
        <div class="panel panel-success col-sm-10 margLeft translucentwhite pad50">
            <center>
            <div>
                <img src="images/error.gif" id="errorImage" class="img-responsive">
            </div></center>
            <div class="firstObject parBlack simpleEngel">
                <h3><b><span class="fa fa-warning"></span> This content is currently unavailable</b></h3>
            </div><hr>
            <div class="parBlack simpleEngel">
                <h4>The page you requested cannot be displayed right now. It may be temporarily unavailable, 
                    the link you clicked on may have expired, or you may not have permission to view this page.</h4>    
            </div>
        </div>
    </div>
    
	<!--FOOTER-->
	<%@include file="styles/commonFooter.html" %>
    
	<!-- Scripts -->
	<%@include file="styles/scripts.html" %>

</body>
</html>