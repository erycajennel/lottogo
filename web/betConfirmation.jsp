<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="net.tanesha.recaptcha.ReCaptcha" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaFactory" %>
<%@ page import="com.iacademy.LottoGo.beans.Bet" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>LottoGo | Sign up</title>
	<%@include file="styles/importCss.html" %>
</head>
<body>    
	<%
	HttpSession userSession = request.getSession();
	String sessionUser = (String)userSession.getAttribute("userName");
	String membership = (String)userSession.getAttribute("membership");
	Bet lastBet = (Bet)userSession.getAttribute("bet");
	if(lastBet == null){
	%>

    <!--BLOCK FOR NAVBAR-->
    <div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<a href="home.jsp" class="navbar-brand pull-left"><img src="images/Logo.png" id="logoHeader"></a>

			<button class="navbar-toggle" data-toggle="collapse" data-target=".navHeaderCollapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>

			<div class = "collapse navbar-collapse navHeaderCollapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="home.jsp"><span class="glyphicon glyphicon-home"></span> Home</a></li>
					<li><a href="rules.jsp"><span class="glyphicon glyphicon-bullhorn"></span> Rules</a></li>
					<li><a href="pcso.jsp"><span class="glyphicon glyphicon-check"></span> PCSO Accreditation</a></li>
					<li><a href="about.jsp"><span class="glyphicon glyphicon-info-sign"></span> About</a></li>
					<li><a href="play.jsp"><span class="glyphicon glyphicon-pencil"></span>
							Play!</a></li>
					<li class="dropdown"><a class="dropdown-toggle"
						data-toggle="dropdown" href="#"><span
							class="glyphicon glyphicon-user"></span> Welcome, <%=sessionUser%><span
							class="caret"></span> </a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="logout"><span class="fa fa-sign-out"></span>Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
	<!--SignUp Form-->
	<div class="jumbotron specialJumbo firstObject">
		<div class="container-fluid">
			<h1 class="inLineHeading">One step left!</h1>
		</div><br>
		
		<div class="container well well-sm">
			<div class="form-horizontal wrapper-signup" role="form">
				<center><br><br>
				<div class="alert alert-success" role="alert"><b>Congratulations!</b> Your bet has been registered.</div>
				<div>
					<button href="play.jsp" class="btn btn-danger btn-block">Bet Again!</button>
					<%userSession.setAttribute("bet", null); %>
				</div>
				</center>
			</div>
		</div>
		
	</div>	
    
	<!-- Scripts -->
    <%@include file="styles/scripts.html" %>
    
	<%}else{
		response.sendRedirect("error.jsp");
	} %>
</body>
</html>