<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.sql.Connection"%>
<%@ page import="com.iacademy.LottoGo.database.*"%>
<%@ page import="java.sql.ResultSet"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>LottoGo | Play</title>
	<%@include file="styles/importCss.html" %>
</head>
<body>
	<%
	String membership = (String)session.getAttribute("membership");
	if(membership != null && membership.equals("member")){
	%>
    <image src="images/brownBlueBg.png" id="HomeBG"/>
    <!--BLOCK FOR NAVBAR-->
    <div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<a href="home.jsp" class="navbar-brand pull-left"><img src="images/Logo.png" id="logoHeader"></a>

			<button class="navbar-toggle" data-toggle="collapse" data-target=".navHeaderCollapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>

			<div class = "collapse navbar-collapse navHeaderCollapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="home.jsp"><span class="glyphicon glyphicon-home"></span> Home</a></li>
					<li><a href="rules.jsp"><span class="glyphicon glyphicon-bullhorn"></span> Rules</a></li>
					<li><a href="pcso.jsp"><span class="glyphicon glyphicon-check"></span> PCSO Accreditation</a></li>
					<li><a href="about.jsp"><span class="glyphicon glyphicon-info-sign"></span> About</a></li>
					<%@include file="styles/navExtension.jsp" %>
				</ul>
			</div>
		</div>
	</div>
	
	<!--Carousel-->
	<div id="myCarousel" class="carousel slide">
            
		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
			<li data-target="#myCarousel" data-slide-to="2"></li>
		</ol>

		<div class="carousel-inner">
			<div class="item active">
				<img src="images/rule1.jpg" alt="rule1" class="img-responsive">
			</div>

			<div class="item">
				<img src="images/rule2.jpg" alt="rule2" class="img-responsive">
			</div>

			<div class="item">
				<img src="images/rule3.jpg" alt="rule3" class="img-responsive">
			</div>
		</div>
		<a class="carousel-control left" href="#myCarousel" data-slide="prev">
			<span class="icon-prev"></span></a>
		<a class="carousel-control right" href="#myCarousel" data-slide="next">
			<span class="icon-next"></span></a>
	</div><br><br>
	
	<!--FORM FOR GAME-->
	<div id="mainContainer" class="container col-lg-12 lastObject">
		<div class="container col-sm-offset-1 col-sm-5 form-horizontal blackBg" role="form">
			<center><h1 class="inLineHeading col-sm-offset-1 parBlack">Place Your bets!</h1>
			<form action = "processGame" method="post">					
				<div class="input-group col-sm-8">
					<span class="input-group-addon">1</span>
					<input type="text" class="form-control" name="1stNum" placeholder="Enter 1st number" value="${firstNumber}">
				</div><br>
				<div class="input-group col-sm-8">
					<span class="input-group-addon">2</span>
					<input type="text" class="form-control" name="2ndNum" placeholder="Enter 2nd number" value="${secondNumber}">
				</div><br>
				<div class="input-group col-sm-8">
					<span class="input-group-addon">3</span>
					<input type="text" class="form-control" name="3rdNum" placeholder="Enter 3rd number" value="${thirdNumber}">
				</div><br>
				<div class="input-group col-sm-8">
					<span class="input-group-addon">4</span>
					<input type="text" class="form-control" name="4thNum" placeholder="Enter 4th number" value="${fourthNumber}">
				</div><br>
				<div class="input-group col-sm-8">
					<span class="input-group-addon">5</span>
					<input type="text" class="form-control" name="5thNum" placeholder="Enter 5th number" value="${fifthNumber}">
				</div><br>
				<div class="input-group col-sm-8">
					<span class="input-group-addon">6</span>
					<input type="text" class="form-control" name="6thNum" placeholder="Enter 6th number" value="${sixthNumber}">
				</div>
				<br>
				
				<center><div class="alert col-sm-12" style="color: #ffc504;" role="alert">${errorMessage}</div></center>
						
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-8">
						<input type="submit" class="btn btn-danger btn-lg btn-block" name="luckyPick" value="Lucky Pick!"/>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-8">
						<input type="submit" class="btn btn-primary btn-lg btn-block" name="submit" value="Submit!"/>
					</div>
				</div>
			</form></center>
		</div>
		
		<%//Get the jackpot prize
			Connection connection = null;
			SqlQuery sqlQuery = new SqlQuery();
	
			connection = DBConnection.getConnection();			
			int jackpotPrize = 0;
			ResultSet rsBets = sqlQuery.rsSelect(connection, "COUNT(bet_id)", "bet", "bet_id");
			while (rsBets.next()) {
				jackpotPrize = 9000000  + rsBets.getInt(1)*10 ;
			}
		%>
		
		<!--SECTION FOR PRIZES-->
		<div class="container col-sm-6">
			<center><h1 class="largeInline">PRIZES</h1>				
			<div class="well well-lg col-sm-offset-2 col-sm-8 blackBg">
				<h1 class="simpleEngel"><b>JACKPOT PRIZE!</b></h1>
				<h1 class="moneyFont"><%=jackpotPrize %></h1>
			</div>
			<div class="col-sm-offset-2 col-sm-8 blackBg">
				<h2 class="moneyFont"><b class="simpleEngel">5-Digit : </b>Php 23,000</h2>
			</div>
			<div class="col-sm-offset-2 col-sm-8 blackBg">
				<h2 class="moneyFont"><b class="simpleEngel">4-Digit : </b>Php 600</h2>
			</div>
			<div class="col-sm-offset-2 col-sm-8 blackBg">
				<h2 class="moneyFont"><b class="simpleEngel">3-Digit : </b>Php 40</h2>
			</div>
			</center>
		</div>
	</div>
	
	<!--FOOTER-->
	<%@include file="styles/commonFooter.html" %>
    
	<!-- Scripts -->
    <%@include file="styles/scripts.html" %>
    <script>
   		$("${focus} :input").focus();
    </script>

	<%}else{
		response.sendRedirect("error.jsp");
	} %>
</body>
</html>