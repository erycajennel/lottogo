<%@page import="java.sql.ResultSet"%>
<%@page import="com.iacademy.LottoGo.database.DBConnection"%>
<%@page import="com.iacademy.LottoGo.database.SqlQuery"%>
<%@page import="com.iacademy.LottoGo.utils.ProcessDraw"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>LottoGo | DRAW</title>
		<%@include file="styles/importCss.html" %>
	</head>
	<body>
    <%
	HttpSession userSession = request.getSession();
	String sessionUser = (String)userSession.getAttribute("userName");
	String membership = (String)userSession.getAttribute("membership");
	if(membership != null && membership.equals("admin")){
	%>
	
	<%-- Get connection --%>
	<% 
		Connection connection = null;
		connection = DBConnection.getConnection();
		SqlQuery sqlQuery = new SqlQuery();
		
	%>	
	
	<%-- Run Queries --%>
	<% 
	
		// Get last draw ID
		
		int last_draw_id= 0; ;
		
		ResultSet rsLastDrawID = sqlQuery.rsSelectLastColumn(connection,
				"winning_number", "draw_id", "draw_id");
			while (rsLastDrawID.next()) {
				last_draw_id = rsLastDrawID.getInt(1);
			}
				
		// Get Winners
		
		String draw_to_display = Integer.toString(last_draw_id);
		ResultSet rsWinners = sqlQuery.rsInnerJoin(connection, "*", "user", "winners", "username", "draw_id = "+draw_to_display);
		
		// compute for jackpot prize
			// get all bets
			int jackpot = 0;
			ResultSet rsBets = sqlQuery.rsSelect(connection, "COUNT(bet_id)", "bet", "bet_id");
			while (rsBets.next()) {
				jackpot = 9000000  + rsBets.getInt(1)*10 ;
			}
	
		
	%>
	
	<image src="images/brownBlueBg.png" id="HomeBG" />

	<!--BLOCK FOR NAVBAR-->
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<a href="adminHome.jsp" class="navbar-brand pull-left"><img
				src="images/Logo.png" id="logoHeader"></a>

			<button class="navbar-toggle" data-toggle="collapse"
				data-target=".navHeaderCollapse">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>

			<div class="collapse navbar-collapse navHeaderCollapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="adminHome.jsp"><span
							class="glyphicon glyphicon-tasks"></span> Home</a></li>
					<li class="active"><a href="draw.jsp"><span
							class="fa fa-tachometer"></span> Draw</a></li>
					<li class="dropdown"><a class="dropdown-toggle"
						data-toggle="dropdown" href="#"><span
							class="glyphicon glyphicon-user"></span> Welcome, <%=sessionUser%><span
							class="caret"></span> </a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="logout"><span class="fa fa-sign-out"></span>Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<br>

	<!--WELCOME MESSAGE-->
	
	<div class="container" id="pageTitle">
		<h1>
			<span class="fa fa-tachometer"></span> Draw Winning Numbers!
		</h1>
	</div>
	<br>
	<br>

	<!--FORM FOR GAME-->
	<div class="container col-lg-12">
		<div
			class="container col-sm-offset-1 col-sm-5 form-horizontal blackBg"
			role="form">
			<center>
				<h1 class="inLineHeading col-sm-offset-2 parBlack">LUCKY DRAW</h1>
				<form name="draw" action="ProcessDraw" method="post" >
					<div class="input-group col-sm-8">
						<span class="input-group-addon">1st Number: </span> <input
							type="text" class="form-control" name="1stNum" value="${firstNumber}" disabled>
					</div>
					<br>
					<div class="input-group col-sm-8">
						<span class="input-group-addon">2nd Number: </span> <input
							type="text" class="form-control" name="2ndNum" value="${secondNumber}" disabled>
					</div>
					<br>
					<div class="input-group col-sm-8">
						<span class="input-group-addon">3rd Number: </span> <input
							type="text" class="form-control" name="3rdNum" value="${thirdNumber}" disabled>
					</div>
					<br>
					<div class="input-group col-sm-8">
						<span class="input-group-addon">4th Number: </span> <input
							type="text" class="form-control" name="4thNum" value="${fourthNumber}" disabled>
					</div>
					<br>
					<div class="input-group col-sm-8">
						<span class="input-group-addon">5th Number: </span> <input
							type="text" class="form-control" name="5thNum" value="${fifthNumber}" disabled>
					</div>
					<br>
					 <div class="input-group col-sm-8">
	                    <span class="input-group-addon">6th Number : </span>
	                    <input type="text" class="form-control" name="6thNum" value="${sixthNumber}" disabled>
	                </div>
	                <br>
	                <div class="form-group">
	                	
                    <div class="col-sm-offset-2 col-sm-8">
                        <input type="submit" name="haha"  class="btn btn-warning btn-lg btn-block" value="Draw!" ${disable} }/>
                    </div>
	                </div>
	            </form></center>
	        </div>

		<!-- PRIZES SECTION -->
		
		<div class="container col-sm-6">
			<center>
			
				<h1 class="largeInline">PRIZES</h1>
				<div class="well well-lg col-sm-offset-2 col-sm-8 blackBg">
					<h1 class="simpleEngel">
						<b>JACKPOT PRIZE!</b>
					</h1>
					<h1 class="moneyFont"> <%= jackpot %></h1>
				</div>
				<div class="col-sm-offset-2 col-sm-8 blackBg">
					<h2 class="moneyFont">
						<b class="simpleEngel">5-Digit : </b>Php 23,000
					</h2>
				</div>
				<div class="col-sm-offset-2 col-sm-8 blackBg">
					<h2 class="moneyFont">
						<b class="simpleEngel">4-Digit : </b>Php 600
					</h2>
				</div>
				<div class="col-sm-offset-2 col-sm-8 blackBg">
					<h2 class="moneyFont">
						<b class="simpleEngel">3-Digit : </b>Php 40
					</h2>
				</div>
			</center>
		</div>
	</div>

	<!--TABLE OF WINNERS-->
		
	<%-- SELECT THE WINNERS FROM THE CURRENT DRAW --%>
	
	
	<div class="col-sm-12 firstObject">
		<div class="panel panel-success col-sm-10 margLeft translucentwhite">
			<!-- Default panel contents -->
			
			<div class="panel panel-heading">
				<h1 class="simpleEngel">Winners</h1>
			</div>
			<!-- Table -->
			
			<div class="scrollable">
				<table class="table">
					<tr>
						<th>Winner ID</th>
						<th>Username</th>
						<th>Name</th>
						<th>Address</th>
						<th>E-mail</th>
						<th>Phone No.</th>
						<th>Cellphone No.</th>
						<th>Bet</th>
						<th>Status</th>
						<th>Amount Won</th> 
					</tr>
					<%
						while (rsWinners.next()) {
					%>
					<tr>
						<td><%=rsWinners.getString("winner_id")%></td>
						<td><%=rsWinners.getString("username")%></td>
						<td><%=rsWinners.getString("first_name") 
						+ " " + rsWinners.getString("last_name")%></td>
						<td><%=rsWinners.getString("address")%></td>
						<td><%=rsWinners.getString("email")%></td>
						<td><%=rsWinners.getString("phone_number")%></td>
						<td><%=rsWinners.getString("cell_number")%></td>
						<td><%=rsWinners.getString("first_number")
						+ " - " + rsWinners.getString("second_number")
						+ " - " + rsWinners.getString("third_number")
						+ " - " + rsWinners.getString("fourth_number")
						+ " - " + rsWinners.getString("fifth_number")
						+ " - " + rsWinners.getString("sixth_number")
						%></td>
						<td><%=rsWinners.getString("status")%></td>
						<td><%=rsWinners.getString("amount_won")%></td>
					</tr>
					<%
						}
					%>
				</table>
			</div>
		</div>
	</div>

	
	<!--PDF DOWNLOAD BUTTON-->
	<div class = "lastObject pull-right margRight">
	 	<form NAME="download" ACTION="PDFReport" METHOD="POST">
           	<button class="btn btn-danger btn-lg" name="download">
				<span class="glyphicon glyphicon-cloud-download"></span> Download PDF
				Report
			</button>
       	</form>
	</div>

	<!--FOOTER-->
	<%@include file="styles/commonFooter.html"%>

	<!-- MODAL -->
	<%@include file="login.jsp"%>

	<!-- Scripts -->
	<%@include file="styles/scripts.html"%>
	<%}else{
		response.sendRedirect("error.jsp");
	} %>
	</body>
</html>