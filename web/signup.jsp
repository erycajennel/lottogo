<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>LottoGo | Sign up</title>
	<%@include file="styles/importCss.html" %>
	<script type="text/javascript" src="js/pass.js"></script>
</head>
<body>    
	<%
	String sessionUser = (String)session.getAttribute("userName");
	String membership = (String)session.getAttribute("membership");
	if(membership == null){
	%>
    <!--BLOCK FOR NAVBAR-->
    <div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<a href="home.jsp" class="navbar-brand pull-left"><img src="images/Logo.png" id="logoHeader"></a>
		</div>
	</div>
	
	<!--SignUp Form-->
	<div class="jumbotron specialJumbo firstObject">
		<div class="container-fluid">
			<h1 class="inLineHeading">Start Your Lucky Day Today!</h1>
		</div>
		
		<div class="container well well-sm">
			<div class="form-horizontal wrapper-signup" role="form">
				<center><br><br>
				<form id="signup" action = "SignupValidation" method="post">			
					<div class="alert col-sm-10" style="color: #FF0000;" role="alert">${errorMessage}</div>
							
					<!--LAST NAME-->
					<div class="form-group">
						<label for="lastname" class="col-sm-3 control-label">Last Name : </label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="lastname" name="lastName" value="${lastName}" required>
						</div>
					</div>

					<!--FIRST NAME-->
					<div class="form-group">
						<label for="firstname" class="col-sm-3 control-label">First Name : </label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="firstname" name="firstName" value="${firstName}" required>
						</div>
					</div>

					<!--BIRTH DATE-->
					<div class="form-group">
						<label for="birthdate" class="col-sm-3 control-label">Date of Birth : </label>
						<div class="col-sm-4">
							<input type="date" class="form-control" id ="birthdate" name="birthDate" value="${birthDate}" required>
						</div>
					</div>
					
					<!--ADDRESS-->
					<div class="form-group">
						<label for="address" class="col-sm-3 control-label">Address : </label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="address" name="address" value="${address}">
						</div>
					</div>
					
					<!--PHONE NO.-->
					<div class="form-group">
						<label for="phoneno" class="col-sm-3 control-label">Phone No. : </label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="phoneno" name="phoneNo" value="${phoneNo}" placeholder="911 11 11">
						</div>
					</div>
					
					<!--CELL NO.-->
					<div class="form-group">
						<label for="cellno" class="col-sm-3 control-label">Cell No. : </label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="cellno" name="cellNo" value="${cellNo}" placeholder="09123456789">
						</div>
					</div>
					
					<!--EMAIL-->
					<div class="form-group">
						<label for="email" class="col-sm-3 control-label">Email : </label>
						<div class="col-sm-4">
							<input type="email" class="form-control" id="email" name="userEmail" value="${userEmail}" placeholder="you@example.com" required>
						</div>
					</div>
					
					<br><hr><br>
					
					<!--USERNAME-->
					<div class="form-group">
						<label for="username" class="col-sm-3 control-label">User Name : </label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="username" name="userName" value="${userName}" required>
						</div>
					</div>
					
					<!--PASSWORD-->
					<div class="form-group">
						<label for="password" class="col-sm-3 control-label">Password : </label>
						<div class="col-sm-4">
							<input type="password" class="form-control" id="password" name="password" value="${password}" placeholder="Minimum of 8 characters" onkeyup='CheckPasswordStrength(this.value);'required><div id='pwd_strength'></div>
						</div>
					</div>
					
					<!--CONFIRM PASSWORD-->
					<div class="form-group">
						<label for="conpass" class="col-sm-3 control-label">Confirm Password : </label>
						<div class="col-sm-4">
							<input type="password" class="form-control" id="conpass" name="conPassword" value="${conPassword}" placeholder="Minimum of 8 characters" required>
						</div>
					</div><br><br>
					
					<div class="form-group">
						<div class="col-sm-10">
							<%
							ReCaptcha signCaptcha = ReCaptchaFactory.newReCaptcha(
	     		  					"6LdlHOsSAAAAAM8ypy8W2KXvgMtY2dFsiQT3HVq-", 
	      						"6LdlHOsSAAAAACe2WYaGCjU2sc95EZqCI9wLcLXY", false);
							out.print(signCaptcha.createRecaptchaHtml(null, null));
							%>
						</div>
					</div>
					
					<div class="alert col-sm-10" style="color: #FF0000;" role="alert">${errorMessage}</div>
					
					<div class="form-group">
						<div class="col-sm-12">
							<input type="submit" class="btn btn-info btn-lg btn-block" value="Sign me up!"/>
							<!--input type="reset" class="btn btn-default col-sm- " value="Reset"/-->
						</div>
					</div>
				</form></center>
			</div>
		</div>
		
	</div>	
    
    <!--MODALS (Popup Box for Login/SignUp)-->
	<%@include file="login.jsp" %>
    
	<!-- Scripts -->
    <%@include file="styles/scripts.html" %>
	
	
	<%}else{
		response.sendRedirect("error.jsp");
	}%>
</body>
</html>