<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>LottoGo | How To Play</title>
		<%@ include file="styles/importCss.html"%>
	</head>
	<body>
	    <image src="images/brownBlueBg.png" id="HomeBG"/>
	    
	    <!--BLOCK FOR NAVBAR-->
	    <div class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
				<a href="home.jsp" class="navbar-brand pull-left"><img src="images/Logo.png" id="logoHeader"></a>
	
				<button class="navbar-toggle" data-toggle="collapse" data-target=".navHeaderCollapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
	
				<div class = "collapse navbar-collapse navHeaderCollapse">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="home.jsp"><span class="glyphicon glyphicon-home"></span> Home</a></li>
						<li class="active"><a href="rules.jsp"><span class="glyphicon glyphicon-bullhorn"></span> Rules</a></li>
						<li><a href="pcso.jsp"><span class="glyphicon glyphicon-check"></span> PCSO Accreditation</a></li>
						<li><a href="about.jsp"><span class="glyphicon glyphicon-info-sign"></span> About</a></li>
						<%@include file="styles/navExtension.jsp" %>
					</ul>
				</div>
			</div>
		</div><br>
		
		<!--TOP BOX WITH INFO AND BUTTONS-->
		<div class="container" id="pageTitle">
			<h1><span class="glyphicon glyphicon-bullhorn"></span> Rules of The Game</h1>
			<p class="questionLine"><i>The 4 easy steps to a brighter future!</i></p>
		</div>
	
		<!--RESPONSIVE GRID SYSTEM-->
		<div class="container panel-body">
			<div class="row">
				<div class="col-md-3 parBlack"> <!--column, medium, 3 columns covered-->
					<h2><b>Step 1:</b></h2>
					<h3><b>Sign up to LottoGo</b></h3>
					<p>To join, simply sign up with your name, email and contact details. You must be above 17 yrs. old to play the game.</p>
					<p class="questionLine">Why do we need this information?</p>
					<p>LottoGo will use this information to contact and verify selected winner and send prizes to the correct person at the correct address.</p>
				</div>
	
				<div class="col-md-3 parBlack"> <!--column, medium, 3 columns covered-->
					<h2><b>Step 2:</b></h2>
					<h3><b>Play!</b></h3>
					<p>Once logged in, you will be redirected to the page where you can play the game.</p>
					<p class="questionLine">How does it work?</p>
					<p>Given the numbers 1-45, you are to choose ONLY 6 non-repeating numbers. Enter to the given text fields all of the 6 chosen numbers.</p>
					<p class="questionLine">What if I don't have any numbers in mind?</p>
					<p>Lucky for you, LottoGo can pick all lucky numbers for you by clicking the 'Lucky Pick' at the end of the form.</p>
				</div>
	
				<div class="col-md-3 parBlack"> <!--column, medium, 3 columns covered-->
					<h2><b>Step 3:</b></h2>
					<h3><b>Click Submit!</b></h3>
					<p>Once you have finalized your lucky numbers, click the submit button to submit your form to register it to our database.</p>
				</div>
	
				<div class="col-md-3 parBlack"> <!--column, medium, 3 columns covered-->
					<h2><b>Step 4:</b></h2>
					<h3><b>Pay</b></h3>
					<p>To complete the process, all you have to do is pay for the ticket. For just 10Php!</p>
					<p class="questionLine">How would I pay?</p>
					<p>You are given two choices by either using your PayPal account, or by using any of your credit cards.</p>
					<p>That's it! All you have to do is to wait for the draws held every mondays, wednesdays, and fridays of the week!</p>
				</div>
			</div>
		</div>
	    
		<!--FOOTER-->
		<%@ include file="styles/commonFooter.html" %>
		
	    <!--MODALS (Popup Box for Login/SignUp)-->
		<%@ include file="login.jsp" %>
	    
		<!-- Scripts -->
	    <%@ include file="styles/scripts.html" %>
	
	</body>
</html>