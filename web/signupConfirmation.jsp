<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="net.tanesha.recaptcha.ReCaptcha" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaFactory" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>LottoGo | Sign up</title>
	<%@include file="styles/importCss.html" %>
</head>
<body>    
	<%
	String sessionUser = (String)session.getAttribute("userName");
	String membership = (String)session.getAttribute("membership");
	if(membership == null){
	%>

    <!--BLOCK FOR NAVBAR-->
    <div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<a href="home.jsp" class="navbar-brand pull-left"><img src="images/Logo.png" id="logoHeader"></a>

			<button class="navbar-toggle" data-toggle="collapse" data-target=".navHeaderCollapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
	</div>
	
	<!--SignUp Form-->
	<div class="jumbotron specialJumbo firstObject">
		<div class="container-fluid">
			<h1 class="inLineHeading">One step left!</h1>
		</div><br>
		
		<div class="container well well-sm">
			<div class="form-horizontal wrapper-signup" role="form">
				<center><br><br>
				<div class="alert alert-success" role="alert"><b>Congratulations!</b> A mail has been sent to your email. Please check it to verify your account.</div>
				<div></div>
				</center>
			</div>
		</div>
		
	</div>	
    
	<!-- Scripts -->
    <%@include file="styles/scripts.html" %>
    
	<%}else{
		response.sendRedirect("error.jsp");
	} %>
</body>
</html>