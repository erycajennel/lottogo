<%@ page import="com.iacademy.LottoGo.database.*"%>
<%@page import="java.sql.Connection"%>
<%@ page import="java.sql.ResultSet"%>
<%
	Connection conn = null;
	SqlQuery newQuery = new SqlQuery();
	
	conn = DBConnection.getConnection();
	int drawId = 0;
	
	ResultSet rsWinNum = newQuery.rsSelectLastEntry(conn,
		"winning_number", "draw_id");
	while(rsWinNum.next()){
		drawId = rsWinNum.getInt(1);
	}
	System.out.print(drawId);
	ResultSet winResult = newQuery.rsGetCount(conn, "winner_id", "winners", "draw_id = '" + drawId + "'");
	int numWinners = 0;
	while(winResult.next()){
		numWinners = winResult.getInt(1);
	}
%>

<div class="modal fade bs-example-modal-sm" id="winners" role="dialog">
	<div class="modal-dialog modal-sm">
		<center><div class="modal-header modal-header-inverse">
			<h2><b>No. of</b></h2>
			<h2><b>Previous</b></h2>
			<h2><b>Winners</b></h2>
		</div></center>
	    <div class="modal-body modal-body-inverse blackBg">
			<center><h1 class="moneyFont largeInline"><%=numWinners %></h1></center>
		</div>
	</div>
</div>

<div class="modal fade bs-example-modal-sm" id="lottoInfo" role="dialog">
	<div class="modal-dialog">
		<div class="modal-header modal-header-inverse">
			<center><h2><b>The Lotto Game</b></h2></center>
		</div>
	    <div class="modal-body modal-body-inverse blackBg" id="lottoText">
			<p>	Lotto is the nation's biggest lottery game and has made more than 2,300 millionaires since it started in March 8, 1995 and involves the PCSO workforce consisting of more than 2,000 employees nationwide. With hundreds of thousands of players winning cash prizes every week, what would you do with a Lotto jackpot?</p>
			<p>	Mega Lotto 6/45 is a more improved version of the 6/42 draw and is also introduced as nationwide one. As the name states, a six-number combination is chosen from a lot of numbers from 1 to 45. As with 6/42, at least three of one's chosen numbers must appear among the six winning numbers to win a prize. The odds of getting all six winning numbers are much larger at 1 in 8,145,060. Draws are held on Mondays, Wednesdays, and Fridays.</p>
		</div>
	</div>
</div>