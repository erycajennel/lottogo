<%@ page import="com.iacademy.LottoGo.utils.AESencrp"%>
<%
HttpSession userSession = request.getSession();
String sessionUser = (String)session.getAttribute("userName");
String sessionMem = (String)session.getAttribute("membership");

Cookie[] allCookies = request.getCookies();
Cookie userCookie = null;
Cookie memCookie = null;

// iterate all the cookie array and look for the target cookie created
if (sessionUser != null || allCookies != null) {
	
	for (Cookie cookie : allCookies) {
		if (cookie.getName().equals("lottoGoUser")) {
			userCookie = cookie;
			String usernameDec = AESencrp.decrypt(userCookie.getValue());
			userSession.setAttribute("userName", usernameDec);
			sessionUser = (String)userSession.getAttribute("userName");
			userSession.setMaxInactiveInterval(10* 60);
		}
		if (cookie.getName().equals("lottoGoMember")){
			memCookie = cookie;
			String memberDec = AESencrp.decrypt(memCookie.getValue());
			userSession.setAttribute("membership", memberDec);
			sessionMem = (String)userSession.getAttribute("membership");
			userSession.setMaxInactiveInterval(10* 60);
		}
	}

	if (sessionUser != null && sessionMem.equals("member")) {%>
		<li><a href="play.jsp"><span class="glyphicon glyphicon-pencil"></span>
				Play!</a></li>
		<li class="dropdown"><a class="dropdown-toggle"
			data-toggle="dropdown" href="#"><span
				class="glyphicon glyphicon-user"></span> Welcome, <%=sessionUser%><span
				class="caret"></span> </a>
			<ul class="dropdown-menu" role="menu">
				<li><a href="logout"><span class="fa fa-sign-out"></span>Logout</a></li>
			</ul></li>
	<%} else if(sessionMem != null && sessionMem.equals("admin")){
			response.sendRedirect("adminHome.jsp");
	}else {%>
		<li><a href="#login" data-toggle="modal"><span
		class="glyphicon glyphicon-pencil"></span> Play!</a></li>
	<%}
} else {%>
	<li><a href="#login" data-toggle="modal"><span
			class="glyphicon glyphicon-pencil"></span> Play!</a></li>
<%}%>