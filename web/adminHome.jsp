<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="com.iacademy.LottoGo.database.DBConnection"%>
<%@page import="com.iacademy.LottoGo.database.SqlQuery"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>LottoGo | ADMIN</title>
        <%@ include file="styles/importCss.html"%>
    </head>
    <body>
    <%
	HttpSession userSession = request.getSession();
	String sessionUser = (String)userSession.getAttribute("userName");
	String membership = (String)userSession.getAttribute("membership");
	if(membership != null && membership.equals("admin")){
	%>
        <%-- Get connection --%>
	<% 
		Connection connection = null;
		connection = DBConnection.getConnection();
		SqlQuery sqlQuery = new SqlQuery();
	%>	
	
	<%-- Run Queries --%>
	<% 
		// Get last draw ID
		
		int last_draw_id= 0; ;
		ResultSet rsLastDrawID = sqlQuery.rsSelectLastColumn(connection,
				"winning_number", "draw_id", "draw_id");
			while (rsLastDrawID.next()) {
				last_draw_id = rsLastDrawID.getInt(1);
			}
			
			
		// Get current bettors
		ResultSet rsBettors = sqlQuery.rsSelectCondition(connection,
				"*", "bet", " draw_id >"+last_draw_id);

		// Get members
		ResultSet rs = sqlQuery.rsSelectCondition(connection, "*", "user", " membership != 'admin'");

	%>
	
	<image src="images/brownBlueBg.png" id="HomeBG" />

	<!--BLOCK FOR NAVBAR-->


	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<a href="adminHome.jsp" class="navbar-brand pull-left"><img
				src="images/Logo.png" id="logoHeader"></a>

			<button class="navbar-toggle" data-toggle="collapse"
				data-target=".navHeaderCollapse">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>

			<div class="collapse navbar-collapse navHeaderCollapse">
				<ul class="nav navbar-nav navbar-right">
					<li class="active"><a href="adminHome.jsp"><span
							class="glyphicon glyphicon-tasks"></span> Home</a></li>
					<li><a href="draw.jsp"><span class="fa fa-tachometer"></span>
							Draw</a></li>
					<li class="dropdown"><a class="dropdown-toggle"
						data-toggle="dropdown" href="#"><span
							class="glyphicon glyphicon-user"></span> Welcome, <%=sessionUser%><span
							class="caret"></span> </a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="logout"><span class="fa fa-sign-out"></span>Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<br>

	<!--WELCOME MESSAGE-->
	<div class="container" id="pageTitle">
		<h1>
			<span class="glyphicon glyphicon-tasks"></span> Welcome Admin!
		</h1>
	</div>

	<!-------------------------Tables-------------------------->
	<div class="firstObject col-sm-12">
		<div class="panel panel-success col-sm-10 margLeft translucentwhite">
			<!-- Default panel contents -->
			<div class="panel panel-heading">
				<h1 class="simpleEngel">Current Bettors</h1>
			</div>
			<!-- Table -->
			<div class="scrollable">
				<table class="table">
					<tr>
						<th>Username</th>
						<th>Bet</th>
						<th>DrawID</th>
					</tr>
					<%
						while (rsBettors.next()) {
					%>
					<tr>
						<td><%=rsBettors.getString("username")%></td>
						<td><%=rsBettors.getString("first_number")
						+ " - " + rsBettors.getString("second_number")
						+ " - " + rsBettors.getString("third_number")
						+ " - " + rsBettors.getString("fourth_number")
						+ " - " + rsBettors.getString("fifth_number")
						+ " - " + rsBettors.getString("sixth_number")
						%></td>
						<td><%=rsBettors.getString("draw_id")%></td>
					</tr>
					<%
						}
					%>

				</table>
			</div>
		</div>
	</div>

	<!-- SECOND TABLE -->
	<div class="firstObject lastObject col-sm-12">
		<div class="panel panel-success col-sm-10 margLeft translucentwhite">
			<!-- Default panel contents -->
			<div class="panel panel-heading">
				<h1 class="simpleEngel">All Members</h1>
			</div>

			<!-- Table -->
			<div class="scrollable">
				<table class="table">
					<tr>
						<th>Username</th>
						<th>Name</th>
						<th>Address</th>
						<th>E-mail</th>
						<th>Phone Number</th>
						<th>Cellphone Number</th>
					</tr>
					<%
						while (rs.next()) {
					%>
					<tr>
						<td><%=rs.getString("username")%></td>
						<td><%=rs.getString("first_name") 
						+ " " + rs.getString("last_name")%></td>
						<td><%=rs.getString("address")%></td>
						<td><%=rs.getString("email")%></td>
						<td><%=rs.getString("phone_number")%></td>
						<td><%=rs.getString("cell_number")%></td>
					</tr>
					<%
						}
					%>
				</table>
			</div>
		</div>
	</div>

	<!--FOOTER-->
	<%@include file="styles/commonFooter.html"%>

	<!--MODALS (Popup Box for Login/SignUp)-->
	<%@include file="login.jsp"%>

	<!-- Scripts -->
	<%@include file="styles/scripts.html"%>
		<%}else{
		response.sendRedirect("error.jsp");
		} %>
    </body>
</html>
