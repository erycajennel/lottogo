<!-- Login Modal -->
<%@ page import="net.tanesha.recaptcha.ReCaptcha" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaFactory" %>

<div class="modal fade bs-example-modal-sm" id="login" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<%
			HttpSession checkSession = request.getSession();
			String checkSess = (String)checkSession.getAttribute("membership");
			if(checkSess == null){
			%>
			<form id = "loginForm" class="form-horizontal" role="form" action="ProcessLogin" method="post">
			<%}else{%>
			<form id = "loginForm" class="form-horizontal" role="form" action="error.jsp" method="post">
			<%} %>
				<center><div class="modal-header modal-header-signup">
					<h2><b>Login first to play Lotto</b></h2>
				</div></center>
				
			   <div class="modal-body">

					<div class="form-group">
						<label for="username" class="col-sm-3 control-label"><span class="glyphicon glyphicon-user"></span> Username: </label>
					<div class="col-sm-8">
							<input type="text" class="form-control" name="username" id="username" placeholder="Username" required>
						</div>
					</div>

					<div class="form-group">
						<label for="userPassword" class="col-sm-3 control-label"><span class="glyphicon glyphicon-lock"></span> Password: </label>
						<div class="col-sm-8">
							<input type="password" class="form-control" name="userPassword" id="userPassword" placeholder="Password" required>
						</div>
					</div>
				   
					<div class="form-group">
						<div class="col-sm-offset-1 col-sm-10">
							<div class="checkbox">
								<label>
									<input type="checkbox" name="remember" value="true">Remember me
								</label>
							</div>
						</div>
					</div>
					
					<!-- CAPTCHA -->
					<div class="form-group">
						<div class="col-sm-offset-1 col-sm-10">
							<%
							ReCaptcha c = ReCaptchaFactory.newReCaptcha(
	     		  					"6LdlHOsSAAAAAM8ypy8W2KXvgMtY2dFsiQT3HVq-", 
	      						"6LdlHOsSAAAAACe2WYaGCjU2sc95EZqCI9wLcLXY", false);
							out.print(c.createRecaptchaHtml(null, null));
							%>
						</div>
					</div>

					<div class="alert col-sm-offset-1 col-sm-10" style="color: #FF0000;" role="alert">${errorMessage}</div>
					
					<div class="form-group">
						<div class="col-sm-offset-1 col-sm-10">
							<button id="loginButton" class="btn btn-block btn-primary" type="submit">Login</button>
						</div>                                
					</div>
					
				</div>

				<div class="modal-footer">
					<a class="btn pull-left" href="signup.jsp"><span class=""></span>&#43; Create New Account</a>
					<a class="btn btn-basic" data-dismiss="modal">Close</a>
				</div>
			</form>
		</div>
	</div>
</div>