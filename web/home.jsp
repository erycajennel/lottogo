<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.iacademy.LottoGo.database.*"%>
<%@ page import="java.sql.ResultSet"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>LottoGo</title>
	<%@ include file="styles/importCss.html"%>
	<%@ include file="styles/twitterScript.jsp" %>
</head>
<body>
	<%
	    Integer hitsCount = (Integer)application.getAttribute("hitCounter");
	    if( hitsCount == null || hitsCount == 0 ){
	       // First visit 
	       hitsCount = 1;
	    }else{
	       // return visit 
	       hitsCount += 1;
	    }
	    application.setAttribute("hitCounter", hitsCount);
	    
		Connection connection = null;
		SqlQuery sqlQuery = new SqlQuery();

		connection = DBConnection.getConnection();
		
		//GET the last entry of the winning num table
		ResultSet rs = sqlQuery.rsSelectLastEntry(connection,
				"winning_number", "draw_id");
	%>
	<image src="images/brownBlueBg.png" id="HomeBG" />

	<!--BLOCK FOR NAVBAR-->
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<a href="home.jsp" class="navbar-brand pull-left"><img
				src="images/Logo.png" id="logoHeader"></a>

			<button class="navbar-toggle" data-toggle="collapse"
				data-target=".navHeaderCollapse">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>

			<div class="collapse navbar-collapse navHeaderCollapse">
				<ul class="nav navbar-nav navbar-right">
					<li class="active"><a href="home.jsp"><span
							class="glyphicon glyphicon-home"></span> Home</a></li>
					<li><a href="rules.jsp"><span
							class="glyphicon glyphicon-bullhorn"></span> Rules</a></li>
					<li><a href="pcso.jsp"><span
							class="glyphicon glyphicon-check"></span> PCSO Accreditation</a></li>
					<li><a href="about.jsp"><span
							class="glyphicon glyphicon-info-sign"></span> About</a></li>
					<%@include file="styles/navExtension.jsp" %>
				</ul>
			</div>
		</div>
	</div>

	<%while (rs.next()) {%>
	<%
	int number1 = rs.getInt(3);
	int number2 = rs.getInt(4);
	int number3 = rs.getInt(5);
	int number4 = rs.getInt(6);
	int number5 = rs.getInt(7);
	int number6 = rs.getInt(8);
	%>
	<!--Home Background-->
	<div id="background">
		<div class="container">
			<ul id="scene" class="scene">
				<li class="layer" id="smallBalls" data-depth="0.50"><img
					src="images/SmallBalls.png" class="img-responsive"></li>
				<li class="layer" id="lottoBalls" data-depth="0.30"><img
					src="images/LottoBalls.png" class="img-responsive"></li>
				<li class="layer" id="num1" data-depth="0.30"><h1>
						<b><%=rs.getInt(3)%></b>
					</h1></li>
				<li class="layer" id="num2" data-depth="0.30"><h1>
						<b><%=rs.getInt(4)%></b>
					</h1></li>
				<li class="layer" id="num3" data-depth="0.30"><h1>
						<b><%=rs.getInt(5)%></b>
					</h1></li>
				<li class="layer" id="num4" data-depth="0.30"><h1><%=rs.getInt(6)%></h1></li>
				<li class="layer" id="num5" data-depth="0.30"><h1><%=rs.getInt(7)%></h1></li>
				<li class="layer" id="num6" data-depth="0.30"><h1><%=rs.getInt(8)%></h1></li>
				<li class="layer" id="logoImage" data-depth="0.16"><img
					src="images/Logo3.png" class="img-responsive"></li>
			</ul>
		</div>
	</div>
	
	<br><br><br>

	<!--WINNER AND INFO BUTTONS-->
	<div id="infoButtons">
		<center>
			<button class="btn btn-circle btn-lg" href="#winners"
				data-toggle="modal">
				<i class="fa fa-trophy"></i>
			</button>
			<button class="btn btn-circle btn-lg" href="#lottoInfo"
				data-toggle="modal">
				<i class="fa fa-info"></i>
			</button>
		</center>
	</div>

	<!--FOOTER-->
	<div class="navbar navbar-fixed-bottom" id="footer">
		<div class="container" id="footerContent">
			<p class="navbar-text pull-left">
				<span id="date_time"></span>
			</p>
			<!-- Number of views as long as the server's running -->
			<p class="navbar-btn btn pull-right" id="numViews"
				data-toggle="tooltip" data-placement="top" title="Number of views">
				<span class="fa fa-eye"></span> <%=hitsCount %>
			</p>
		<!-- 	<p href="http://twitter.com" class="navbar-btn btn pull-right twitter-share-button">
				<span class="fa fa-twitter"></span> -->
			 <p><a href="https://twitter.com/share" class="twitter-share-button pull-right navbar-btn tweetButton" data-text="LottoGo's Latest Winning Numbers are:
				<%=number1 %>, <%=number2%>, <%=number3%>, <%=number4%>, <%=number5%>, <%=number6%>
				Bet Now!
				" data-lang="en">Tweet
			</a></p>
	<%
		}
	%>		 
		</div>
	</div>

	<!--MODALS (Popup Box for Login/SignUp)-->
	<%@ include file="login.jsp"%>

	<!--MODALS (Popup Box for Login/SignUp)-->
	<%@ include file="styles/homeModals.jsp"%>

	<!-- Scripts -->
	<%@ include file="styles/scripts.html"%>
	
	<%
		connection.close();
	%>
</body>
</html>