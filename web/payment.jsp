<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.iacademy.LottoGo.beans.Bet"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>LottoGo | Payment</title>
<%@include file="styles/importCss.html"%>
</head>
<body>
	<%
		Bet currentBet = (Bet) session.getAttribute("currentBet");
		if (currentBet != null) {
	%>
	<img src="images/brownBlueBg.png" id="HomeBG" />

	<!--BLOCK FOR NAVBAR-->
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<a href="." class="navbar-brand pull-left"><img
				src="images/Logo.png" id="logoHeader"></a>
		</div>
	</div>
	<br>

	<div class="col-sm-offset-2 col-sm-8">
		<div class="container firstObject col-sm-7">
			<h1>
				<span class="fa fa-credit-card"></span> Credit Card Payment
			</h1>
		</div>
	
		<div class="container panel panel-success col-sm-8 col-sm-offset- translucentwhite pad50">
			<div class="parBlack simpleEngel">
				<h3>
					<b><span class="glyphicon glyphicon-check"></span> Confirm Payment</b>
				</h3>
			</div>
			<hr>
			<form action="ProcessPayment" method="post">
				<center>
					<div class="input-group col-sm-10">
						<span class="input-group-addon"> Bet </span> <input type="text"
							class="form-control" name="bet"
							value="<%=currentBet.getFirstNum()%> - <%=currentBet.getSecondNum()%> - <%=currentBet.getThirdNum()%> - <%=currentBet.getFourthNum()%> - <%=currentBet.getFifthNum()%> - <%=currentBet.getSixthNum()%>"
							disabled>
					</div>
					<br>

					<div class="input-group col-sm-10">
						<span class="input-group-addon">Amount</span> <input type="text"
							class="form-control" name="amount" value="Php 10.00" disabled>
					</div>
					<br>

					<div class="input-group col-sm-10">
						<span class="input-group-addon">Card No. </span> <input
							type="text" class="form-control" name="cardNumber" required>
					</div>
					<br>

					<div class="input-group col-sm-10">
						<span class="input-group-addon">Expiration Date</span> <input
							type="date" class="form-control" name="expirationDate" required>
					</div>
					<br>
					
					<div class="alert col-sm-12" style="color: #FF0000;" role="alert">${errorMessage}</div>
	
					<div>
						<button type="submit" class="btn btn-primary btn-block btn-lg" name="payButton" value="creditPay">
							<span class="fa fa-money"></span> Pay!
						</button>
					</div>
				</center>
			</form>
		</div>
	
		<!-- PAYPAL BUTTON -->
		<div class="panel panel-success col-sm-4 translucentwhite pad50 paypalButton">
			<div class="parBlack simpleEngel">
				<h3>
					<b><span class="glyphicon glyphicon-check"></span>  Or pay with your PayPal account!</b>
				</h3>
			</div>
			<hr>
			<center>
				<div>
					<%@include file="styles/paypal.html" %>
				</div>
			</center>
		</div>
	</div>

	<!--FOOTER-->
	<%@include file="styles/commonFooter.html"%>

	<!-- Scripts -->
	<%@include file="styles/scripts.html"%>
	<%
		} else {
			response.sendRedirect("error.jsp");
		}
	%>
</body>
</html>