<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>LottoGo | About Us</title>
		<%@ include file="styles/importCss.html" %>
	</head>
	<body>
	    <image src="images/brownBlueBg.png" id="HomeBG"/>
	    
	    <!--BLOCK FOR NAVBAR-->
	    <div class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
				<a href="home.jsp" class="navbar-brand pull-left"><img src="images/Logo.png" id="logoHeader"></a>
	
				<button class="navbar-toggle" data-toggle="collapse" data-target=".navHeaderCollapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
	
				<div class = "collapse navbar-collapse navHeaderCollapse">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="home.jsp"><span class="glyphicon glyphicon-home"></span> Home</a></li>
						<li><a href="rules.jsp"><span class="glyphicon glyphicon-bullhorn"></span> Rules</a></li>
						<li><a href="pcso.jsp"><span class="glyphicon glyphicon-check"></span> PCSO Accreditation</a></li>
						<li class="active"><a href="about.jsp"><span class="glyphicon glyphicon-info-sign"></span> About</a></li>
						<%@include file="styles/navExtension.jsp" %>
					</ul>
				</div>
			</div>
		</div>
		
		<!--TOP BOX WITH INFO AND BUTTONS-->
		<div class="container" id="pageTitle">
			<h1><span class="glyphicon glyphicon-info-sign"></span> About LottoGo</h1>
			<p class="questionLine"><i>LottoGo is made possible by team HAHA.</i></p>
		</div>
	
		<!--ABOUT LOTTO GO-->
		<div class="container panel-body">
			<div class="row">
				<div class="col-md-12 parIndented"> <!--column, medium, 3 columns covered-->
					<p>LottoGo is a vendor of the largest lottery game in the Philippines. Given accreditation by the Philippine Charity Sweepstakes Office (PCSO), LottoGo has catered
						thousands of users, giving them the comfort of playing the game anytime, and anywhere they want. LottoGo started as a school project of the <i>Team HAHA</i> which
						consist of three of the best software engineers of iACADEMY's SE31. Because of LottoGo's intuitive design of both frontend and backend development, the team were
						given the opportunity to launch the site with the help of PCSO.</p>
				</div>
			</div>
		</div>
		
		<!--RESPONSIVE GRID SYSTEM-->
		<div class="container panel-body lastObject">
			<h3><b><span class="fa fa-users"></span> Team HAHA</b></h3><br>
			<div class="row">
				<div class="col-md-4"> <!--column, medium, 3 columns covered-->
					<center><img class="img-circle" src="images/Allen.jpg"></center><br><br>
					<p class="parCenter parBlack">Allen Arcenal</p>
					<p class="parIndented">Database Admin</p>
				</div>
				<div class="col-md-4"> <!--column, medium, 3 columns covered-->
					<center><img class="img-circle" src="images/Eryca.jpg"></center><br><br>
					<p class="parCenter parBlack">Eryca Jennel Basa</p>
					<p class="parIndented">Junior Developer</p>
				</div>
				<div class="col-md-4"> <!--column, medium, 3 columns covered-->
					<center><img class="img-circle" src="images/Noel.jpg"></center><br><br>
					<p class="parCenter parBlack">Jose Noel Pogi</p>
					<p class="parIndented">Senior Developer</p>
				</div>
			</div>
		</div>
		
		<!--FOOTER-->
		<%@include file="styles/commonFooter.html" %>
		
	    <!--MODALS (Popup Box for Login/SignUp)-->
		<%@include file="login.jsp" %>
	    
		<!-- Scripts -->
	    <%@include file="styles/scripts.html" %>
	
	</body>
</html>