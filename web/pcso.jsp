<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>LottoGo | PCSO Accreditation</title>
		<%@ include file="styles/importCss.html" %>
	</head>
	<body>
	    <image src="images/brownBlueBg.png" id="HomeBG"/>
	    
	    <!--BLOCK FOR NAVBAR-->
	    <div class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
				<a href="home.jsp" class="navbar-brand pull-left"><img src="images/Logo.png" id="logoHeader"></a>
	
				<button class="navbar-toggle" data-toggle="collapse" data-target=".navHeaderCollapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
	
				<div class = "collapse navbar-collapse navHeaderCollapse">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="home.jsp"><span class="glyphicon glyphicon-home"></span> Home</a></li>
						<li><a href="rules.jsp"><span class="glyphicon glyphicon-bullhorn"></span> Rules</a></li>
						<li class="active"><a href="pcso.jsp"><span class="glyphicon glyphicon-check"></span> PCSO Accreditation</a></li>
						<li><a href="about.jsp"><span class="glyphicon glyphicon-info-sign"></span> About</a></li>
						<%@include file="styles/navExtension.jsp" %>	
					</ul>
				</div>
			</div>
		</div><br>
		
		<!--TOP BOX WITH INFO AND BUTTONS-->
		<div class="container" id="pageTitle">
			<h1><span class="glyphicon glyphicon-check"></span> PCSO Accreditation</h1>
			<p class="questionLine"><i>This is to Certify LottoGo as an accredited vendor for the PCSO Lottery Game.</i></p>
		</div>
	
		<!--RESPONSIVE GRID SYSTEM-->
		<div class="container panel-body">
			<div class="row">
				<div class="col-md-12 parIndented"> <!--column, medium, 3 columns covered-->
					<p> Lotto Go is  an  accreditated 6/45 Online Lotto Outlet by the PCSO Accrediting Commission. </p>  
						<p>	Certificate number: 1010 </p>
							<p>Effective date: 27th day of August 2014 </p>
							<p>Expires on: 27th day of August 2020 </p>
				</div>
			</div>
		</div>    
		<br><br><br>
		
		<div>
			<img src="images/Logo.png" id="jumboLogo">
			<img src="images/pcso-logo11.png" id="jumboPcso">
		</div>
		
		<!--FOOTER-->
		<%@include file="styles/commonFooter.html" %>
		
	    <!--MODALS (Popup Box for Login/SignUp)-->
		<%@include file="login.jsp" %>
	    
		<!-- Scripts -->
	    <%@include file="styles/scripts.html" %>
	
	</body>
</html>