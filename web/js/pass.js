
function IsEnoughLength(str,length)
{
	if ((str == null) || isNaN(length))
		return false;
	else if (str.length < length)
		return false;
	return true;
	
}

function HasMixedCase(passwd)
{
	if(passwd.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
		return true;
	else
		return false;
}

function HasNumeral(passwd)
{
	if(passwd.match(/[0-9]/))
		return true;
	else
		return false;
}

function HasSpecialChars(passwd)
{
	if(passwd.match(/.[!,@,#,$,%,^,&,*,?,_,~]/))
		return true;
	else
		return false;
}


function CheckPasswordStrength(pwd)
{
	if (IsEnoughLength(pwd,14) && HasMixedCase(pwd) && HasNumeral(pwd) && HasSpecialChars(pwd))
		pass_strength = "<img src='images/strong.jpg'> <b><font style='color:#01a75f'>Strong</font></b>";
	else if (IsEnoughLength(pwd,8) && HasMixedCase(pwd) && (HasNumeral(pwd) || HasSpecialChars(pwd)))
		pass_strength = "<img src='images/good.jpg'> <b><font style='color:#2471f3'>Good</font></b>";
	else if (IsEnoughLength(pwd,8) && HasNumeral(pwd))
		pass_strength = "<img src='images/fair.jpg'> <b><font style='color:#ffc404'>Fair</font></b>";
	else
		pass_strength = "<img src='images/weak.jpg'> <b><font style='color:#d7422c'>Weak</font></b>";

	document.getElementById('pwd_strength').innerHTML = pass_strength;
}


