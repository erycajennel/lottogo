package com.iacademy.LottoGo.exceptions;

import com.iacademy.LottoGo.messages.Message;

@SuppressWarnings("serial")
public class EntryOutOfBoundsException extends Exception implements Message{
	
	public EntryOutOfBoundsException(){
		super(ENTRY_OUT_OF_BOUNDS + TRY_AGAIN);
	}
	
	public EntryOutOfBoundsException(String message){
		super(message);
	}
	
}
