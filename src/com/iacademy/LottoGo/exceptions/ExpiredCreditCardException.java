package com.iacademy.LottoGo.exceptions;

import com.iacademy.LottoGo.messages.Message;

@SuppressWarnings("serial")
public class ExpiredCreditCardException extends Exception implements Message{
	
	public ExpiredCreditCardException(){
		super(EXPIRED_CREDIT_CARD + TRY_NEW_CARD);
	}
	
	public ExpiredCreditCardException(String message){
		super(message);
	}

}
