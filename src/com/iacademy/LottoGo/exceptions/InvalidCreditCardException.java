package com.iacademy.LottoGo.exceptions;

import com.iacademy.LottoGo.messages.Message;

@SuppressWarnings("serial")
public class InvalidCreditCardException extends Exception implements Message{
	
	public InvalidCreditCardException(){
		super(INVALID_CARD + TRY_NEW_CARD);
	}

	public InvalidCreditCardException(String message){
		super(message);
	}
}
