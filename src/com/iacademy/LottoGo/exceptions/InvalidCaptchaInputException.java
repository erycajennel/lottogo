package com.iacademy.LottoGo.exceptions;

import com.iacademy.LottoGo.messages.Message;

@SuppressWarnings("serial")
public class InvalidCaptchaInputException extends Exception implements Message{
	
	public InvalidCaptchaInputException(){
		super(INVALID_CAPTCHA + TRY_AGAIN);
	}
	
	public InvalidCaptchaInputException(String message){
		super(message);
	}
	
}
