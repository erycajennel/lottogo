package com.iacademy.LottoGo.exceptions;

import com.iacademy.LottoGo.messages.Message;

@SuppressWarnings("serial")
public class EmptyFieldsException extends Exception implements Message{
	
	public EmptyFieldsException(){
		super(EMPTY_ENTRIES + TRY_AGAIN);		
	}
	
	public EmptyFieldsException(String message){
		super(message);
	}
	
}
