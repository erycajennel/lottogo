package com.iacademy.LottoGo.exceptions;

import com.iacademy.LottoGo.messages.Message;

@SuppressWarnings("serial")
public class RepeatingNumbersException extends Exception implements Message{

	public RepeatingNumbersException(){
		super(REPEATING_NUMBERS + TRY_AGAIN);
	}
	
	public RepeatingNumbersException(String message){
		super(message);
	}
	
}
