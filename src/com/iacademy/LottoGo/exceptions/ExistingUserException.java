package com.iacademy.LottoGo.exceptions;

import com.iacademy.LottoGo.messages.Message;

@SuppressWarnings("serial")
public class ExistingUserException extends Exception implements Message{
	
	public ExistingUserException(){
		super(USER_NAME_EXISTS + TRY_AGAIN);
	}
	
	public ExistingUserException(String message){
		super(message);
	}
	
}
