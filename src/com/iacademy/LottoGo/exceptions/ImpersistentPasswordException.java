package com.iacademy.LottoGo.exceptions;

import com.iacademy.LottoGo.messages.Message;

@SuppressWarnings("serial")
public class ImpersistentPasswordException extends Exception implements Message{
	
	public ImpersistentPasswordException(){
		super(IMPERSISTENT_PASSWORD + TRY_AGAIN);
	}
	
	public ImpersistentPasswordException(String message){
		super(message);
	}
}
