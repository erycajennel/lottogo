package com.iacademy.LottoGo.exceptions;

import com.iacademy.LottoGo.messages.Message;

@SuppressWarnings("serial")
public class PasswordNotMatchException extends Exception implements Message {
	
	public PasswordNotMatchException(){
		super(PASS_NOT_MATCH + TRY_AGAIN);
	}
	
	public PasswordNotMatchException(String message){
		super(message);
	}
}
