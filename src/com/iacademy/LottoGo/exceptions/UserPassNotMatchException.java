package com.iacademy.LottoGo.exceptions;

import com.iacademy.LottoGo.messages.Message;

@SuppressWarnings("serial")
public class UserPassNotMatchException extends Exception implements Message{
	
	public UserPassNotMatchException(){
		super(USER_PASS_NOT_MATCH + TRY_AGAIN);
	}
	
	public UserPassNotMatchException(String message){
		super(message);
	}
}
