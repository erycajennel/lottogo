package com.iacademy.LottoGo.exceptions;

import com.iacademy.LottoGo.messages.Message;

@SuppressWarnings("serial")
public class AgeRestrictionException extends Exception implements Message{
	
	public AgeRestrictionException(){
		super(AGE_NOT_MET);
	}
	
	public AgeRestrictionException(String message){
		super(message);
	}
}
