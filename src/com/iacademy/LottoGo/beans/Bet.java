package com.iacademy.LottoGo.beans;

public class Bet {
	
	private String userName;
	private int firstNum;
	private int secondNum;
	private int thirdNum;
	private int fourthNum;
	private int fifthNum;
	private int sixthNum;
	private String status;
	private int drawId;
	
	public int getDrawId() {
		return drawId;
	}
	public void setDrawId(int drawId) {
		this.drawId = drawId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getFirstNum() {
		return firstNum;
	}
	public void setFirstNum(int firstNum) {
		this.firstNum = firstNum;
	}
	public int getSecondNum() {
		return secondNum;
	}
	public void setSecondNum(int secondNum) {
		this.secondNum = secondNum;
	}
	public int getThirdNum() {
		return thirdNum;
	}
	public void setThirdNum(int thirdNum) {
		this.thirdNum = thirdNum;
	}
	public int getFourthNum() {
		return fourthNum;
	}
	public void setFourthNum(int fourthNum) {
		this.fourthNum = fourthNum;
	}
	public int getFifthNum() {
		return fifthNum;
	}
	public void setFifthNum(int fifthNum) {
		this.fifthNum = fifthNum;
	}
	public int getSixthNum() {
		return sixthNum;
	}
	public void setSixthNum(int sixthNum) {
		this.sixthNum = sixthNum;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
