package com.iacademy.LottoGo.beans;

public class User {
	private String userName;
	private String lastName;
	private String firstName;
	private String birthDate;
	private String address;
	private int phoneNo;
	private int cellNo;
	private String userEmail;
	private String password;
	private final String membership = "member";
	
	public String getUsername() {
		return userName;
	}
	public void setUsername(String username) {
		this.userName = username;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getPhoneNumber() {
		return phoneNo;
	}
	public void setPhoneNumber(int phoneNumber) {
		this.phoneNo = phoneNumber;
	}
	public int getCellNumber() {
		return cellNo;
	}
	public void setCellNumber(int cellNumber) {
		this.cellNo = cellNumber;
	}
	public String getEmail() {
		return userEmail;
	}
	public void setEmail(String email) {
		this.userEmail = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getMembership() {
		return membership;
	}
}
