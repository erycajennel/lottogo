package com.iacademy.LottoGo.beans;

public class Payment {
	private String userName;
	private int firstNum;
	private int secondNum;
	private int thirdNum;
	private int fourthNum;
	private int fifthNum;
	private int sixthNum;
	private float amount;
	
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getFirstNum() {
		return firstNum;
	}
	public void setFirstNum(int firstNum) {
		this.firstNum = firstNum;
	}
	public int getSecondNum() {
		return secondNum;
	}
	public void setSecondNum(int secondNum) {
		this.secondNum = secondNum;
	}
	public int getThirdNum() {
		return thirdNum;
	}
	public void setThirdNum(int thirdNum) {
		this.thirdNum = thirdNum;
	}
	public int getFourthNum() {
		return fourthNum;
	}
	public void setFourthNum(int fourthNum) {
		this.fourthNum = fourthNum;
	}
	public int getFifthNum() {
		return fifthNum;
	}
	public void setFifthNum(int fifthNum) {
		this.fifthNum = fifthNum;
	}
	public int getSixthNum() {
		return sixthNum;
	}
	public void setSixthNum(int sixthNum) {
		this.sixthNum = sixthNum;
	}
	public float getAmount() {
		return amount;
	}
	public void setAmount(float amount) {
		this.amount = amount;
	}
}
