package com.iacademy.LottoGo.beans;

public class WinningNumber {
	
	private int firstNum;
	private int secondNum;
	private int thirdNum;
	private int fourthNum;
	private int fifthNum;
	private int sixthNum;
	private double jackpotPrize;
	
	public int getFirstNum() {
		return firstNum;
	}
	public void setFirstNum(int firstNum) {
		this.firstNum = firstNum;
	}
	public int getSecondNum() {
		return secondNum;
	}
	public void setSecondNum(int secondNum) {
		this.secondNum = secondNum;
	}
	public int getThirdNum() {
		return thirdNum;
	}
	public void setThirdNum(int thirdNum) {
		this.thirdNum = thirdNum;
	}
	public int getFourthNum() {
		return fourthNum;
	}
	public void setFourthNum(int fourthNum) {
		this.fourthNum = fourthNum;
	}
	public int getFifthNum() {
		return fifthNum;
	}
	public void setFifthNum(int fifthNum) {
		this.fifthNum = fifthNum;
	}
	public int getSixthNum() {
		return sixthNum;
	}
	public void setSixthNum(int sixthNum) {
		this.sixthNum = sixthNum;
	}
	public double getJackpotPrize() {
		return jackpotPrize;
	}
	public void setJackpotPrize(double jackpotPrize) {
		this.jackpotPrize = jackpotPrize;
	}
	
}
