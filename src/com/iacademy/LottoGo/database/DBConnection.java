package com.iacademy.LottoGo.database;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;


public class DBConnection {
	public static final String DRIVER_CLASS = "com.mysql.jdbc.Driver";
	public static final String URL = "jdbc:mysql://localhost/lottogo";
	public static final String USER = "root";
	public static final String PASSWORD = "";
	private static DBConnection instance = new DBConnection();
	
	private DBConnection(){
		try {
			Class.forName(DRIVER_CLASS);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	private Connection createConnection(){
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(URL, USER, PASSWORD);
		} catch (SQLException e) {
			System.out.println("UNABLE TO ESTABLISH CONNECTION");
			e.printStackTrace();
		}
		return connection;
	}
	
	public static Connection getConnection(){
		return instance.createConnection();
	}	
}
