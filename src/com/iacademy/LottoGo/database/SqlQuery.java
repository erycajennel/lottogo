package com.iacademy.LottoGo.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SqlQuery {

	// ======= insert new users
	public void insertNewUser(Connection conn, String username,
			String last_name, String first_name, String birthday,
			String address, String phone_number, String cell_number,
			String email, String password, String membership) {

		PreparedStatement ps = null;

		String query = "INSERT into User(username, last_name, first_name,"
				+ " birthday, address, phone_number, cell_number, email, password, membership)"
				+ " VALUES (?,?,?,?,?,?,?,?,?,?)";
		try {
			ps = conn.prepareStatement(query);
			ps.setString(1, username);
			ps.setString(2, last_name);
			ps.setString(3, first_name);
			ps.setString(4, birthday);
			ps.setString(5, address);
			ps.setString(6, phone_number);
			ps.setString(7, cell_number);
			ps.setString(8, email);
			ps.setString(9, password);
			ps.setString(10, membership);
			ps.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// ======== insert new bets ==========

	public void insertNewBet(Connection conn, String username, int first_number,
			int second_number, int third_number, int fourth_number,
			int fifth_number, int sixth_number,	String status, int draw_id) {

		PreparedStatement ps = null;

		String query = "INSERT into Bet(username, first_number, second_number, third_number, fourth_number, fifth_number, "
				+ "sixth_number, status, draw_id)"
				+ " VALUES (?,?,?,?,?,?,?,?,?)";
		try {
			ps = conn.prepareStatement(query);
			ps.setString(1, username);
			ps.setInt(2, first_number);
			ps.setInt(3, second_number);
			ps.setInt(4, third_number);
			ps.setInt(5, fourth_number);
			ps.setInt(6, fifth_number);
			ps.setInt(7, sixth_number);
			ps.setString(8, status);
			ps.setInt(9, draw_id);
			ps.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// ======== insert winning number =====

	public void insertQuery(Connection conn, int first_number, int second_number,
			int third_number, int fourth_number, int fifth_number,
			int sixth_number, int jackpot_prize ) {

		PreparedStatement ps = null;

		String query = "INSERT into Winning_number ( "
				+ " first_number, second_number, third_number, fourth_number, fifth_number, "
				+ "sixth_number, jackpot_prize)"
				+ " VALUES (?,?,?,?,?,?,?)";
		try {
			ps = conn.prepareStatement(query);
			ps.setInt(1, first_number);
			ps.setInt(2, second_number);
			ps.setInt(3, third_number);
			ps.setInt(4, fourth_number);
			ps.setInt(5, fifth_number);
			ps.setInt(6, sixth_number);
			ps.setFloat(7, jackpot_prize);
			ps.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	// ===== INSERT WINNERS =====
	// ================================== insert winners
	
	public void insertQuery(Connection conn, String username, int first_number, int second_number,
			int third_number, int fourth_number, int fifth_number,
			int sixth_number, String status, int amount_won, int draw_id ) {

		PreparedStatement ps = null;

		String query = "INSERT into winners ( "
				+ " username, first_number, second_number, third_number, fourth_number, fifth_number, "
				+ "sixth_number, status, amount_won, draw_id)"
				+ " VALUES (?,?,?,?,?,?,?,?,?,?)";
		try {
			ps = conn.prepareStatement(query);
			ps.setString(1, username);
			ps.setInt(2, first_number);
			ps.setInt(3, second_number);
			ps.setInt(4, third_number);
			ps.setInt(5, fourth_number);
			ps.setInt(6, fifth_number);
			ps.setInt(7, sixth_number);
			ps.setString(8, status);
			ps.setInt(9, amount_won);
			ps.setInt(10, draw_id);
			
			ps.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// ========================================== SELECT QUERIES
	// ==============================================================
	
	// SELECT FROM TABLE NAME, ORDER BY ORDER
	public ResultSet rsSelect(Connection conn, String columnName,  String tableName, String order) {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = conn.prepareStatement("SELECT " + columnName + " FROM " + tableName + " ORDER BY " + order);
			rs = ps.executeQuery();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return rs;
	}
	
	

	// SELECT COUNT FROM ANY TABLE
	public ResultSet rsGetCount(Connection conn, String primaryKey,
			String table, String condition) {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = conn.prepareStatement("SELECT COUNT(" + primaryKey + ") FROM "
					+ table + " WHERE " + condition);
			rs = ps.executeQuery();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}

	// SELECT FROM TABLE NAME, ORDER BY ORDER
	public ResultSet rsSelect(Connection conn, String tableName, String order) {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = conn.prepareStatement("SELECT * FROM " + tableName
					+ " ORDER BY " + order);
			rs = ps.executeQuery();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}
	
	public ResultSet rsSelectCondition(Connection conn, String columnName, String tableName, String condition) {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = conn.prepareStatement("SELECT " + columnName + " FROM " + tableName +
					" WHERE " + condition);
			rs = ps.executeQuery();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return rs;
	}
	
	

	// SELECT COLUMN FROM TABLE NAME, ORDER BY ORDER, WHERE COLUMN = CONDITION
	// NOTE : PASS THE OPERATOR "= OR >"
	public ResultSet rsSelectCondition(Connection conn, String columnName,
			String tableName, String condition, String attribute, String order) {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = conn.prepareStatement("SELECT " + columnName + " FROM "
					+ tableName + " WHERE " + columnName + condition + " '"
					+ attribute + "' " + " ORDER BY " + order);
			rs = ps.executeQuery();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}

	// =============== GET ROW WITH 2 CONDITIONS ===============
	public ResultSet rsSelectAccount(Connection conn, String columnName,
			String columnName2, String tableName, String condition,
			String condition2, String attribute1, String attribute2,
			String order) {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = conn.prepareStatement("SELECT " + columnName + ", "
					+ columnName2 + " FROM " + tableName + " WHERE "
					+ columnName + condition + " '" + attribute1 + "' "
					+ " AND " + columnName2 + condition2 + " '" + attribute2
					+ "' " + " ORDER BY " + order);
			rs = ps.executeQuery();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}

	// =============== GET ROW COLUMN ==========================
	public ResultSet rsSelectLastEntry(Connection conn, String tableName,
			String order) {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = conn.prepareStatement("SELECT * FROM " + tableName
					+ " ORDER BY " + order + " DESC LIMIT 1");
			rs = ps.executeQuery();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}

	// =============== LAST ROW WITH COLUMN NAME ===========================
	public ResultSet rsSelectLastColumn(Connection conn, String tableName,
			String columnName, String order) {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = conn.prepareStatement("SELECT " + columnName + " FROM "
					+ tableName + " ORDER BY " + order + " DESC LIMIT 1");
			rs = ps.executeQuery();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}
	
	// ===== INNER JOIN AND UPDATE ===== 
	// INNER JOIN
		// SELECT * FROM user INNER JOIN winners ON user.username = winners.username

	public ResultSet rsInnerJoin(Connection conn, String columnName, String tableName, String tableName2,
			String foreignKey, String condition) {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = conn.prepareStatement("SELECT * FROM " + tableName +
					" INNER JOIN " + tableName2 + " ON " + tableName + "." + foreignKey + " = " +
					tableName2 + "." + foreignKey + " WHERE " + condition );
			
			
			rs = ps.executeQuery();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return rs;
	}
	
	//UPDATE winning_number SET first_number = '11' WHERE draw_id = 1
	public int rsUpdate(Connection conn, String columnName, String value, String tableName, 
			 String condition) {
		PreparedStatement ps = null;
		int rs = 0;

		try {
			ps = conn.prepareStatement("UPDATE " + tableName +
					" SET " + columnName + "= '" + value + "' WHERE " + condition );
			
			
			rs = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return rs;
	}

}
