package com.iacademy.LottoGo.messages;

public interface Message {
	// all variables declared in an interface are implicitly public static final
		String USER_NAME_EXISTS = "Username already exists.";

		String AGE_NOT_MET = "Sorry, but you did not meet the age requirement to play.";
		
		String INVALID_INPUT = "Input invalid.";
		
		String USER_PASS_NOT_MATCH = "Invalid Id or password.";
		
		String INVALID_CAPTCHA = "Wrong input.";
		
		String PASS_NOT_MATCH = "Your password does not match the confirmation password.";
		
		String REPEATING_NUMBERS = "Entry have repeating numbers.";
		
		String ENTRY_OUT_OF_BOUNDS = "Invalid input. Please choose between numbers 1-45.";
		
		String TRY_AGAIN = " Please try again.";
		
		String EMPTY_ENTRIES = "Some fields are empty.";
		
		String IMPERSISTENT_PASSWORD = "Your password is too short.";
		
		String INVALID_CARD = "Your Credit Card is invalid.";
		
		String EXPIRED_CREDIT_CARD = "Your Credit Card is expired";
		
		String TRY_NEW_CARD = "Please try a different credit card";

}
