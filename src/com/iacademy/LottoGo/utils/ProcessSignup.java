package com.iacademy.LottoGo.utils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.tanesha.recaptcha.ReCaptchaImpl;
import net.tanesha.recaptcha.ReCaptchaResponse;

import com.iacademy.LottoGo.beans.User;
import com.iacademy.LottoGo.database.DBConnection;
import com.iacademy.LottoGo.database.SqlQuery;
import com.iacademy.LottoGo.exceptions.AgeRestrictionException;
import com.iacademy.LottoGo.exceptions.EmptyFieldsException;
import com.iacademy.LottoGo.exceptions.ExistingUserException;
import com.iacademy.LottoGo.exceptions.ImpersistentPasswordException;
import com.iacademy.LottoGo.exceptions.PasswordNotMatchException;

@WebServlet("/SignupValidation")
public class ProcessSignup extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");

		// ===== CONNECT DB =====
		Connection connection = null;
		SqlQuery sqlQuery = new SqlQuery();

		connection = DBConnection.getConnection();

		// ===== SETUP RECAPTCHA VALIDATION =====
		String remoteAddr = request.getRemoteAddr();
		ReCaptchaImpl reCaptcha = new ReCaptchaImpl();
		reCaptcha.setPrivateKey("6LdlHOsSAAAAACe2WYaGCjU2sc95EZqCI9wLcLXY");

		String challenge = request.getParameter("recaptcha_challenge_field");
		String uresponse = request.getParameter("recaptcha_response_field");
		ReCaptchaResponse reCaptchaResponse = reCaptcha.checkAnswer(remoteAddr,
				challenge, uresponse);

		String lastName = request.getParameter("lastName");
		String firstName = request.getParameter("firstName");
		String birthDate = request.getParameter("birthDate");
		String address = request.getParameter("address");
		String phoneNo = request.getParameter("phoneNo");
		String cellNo = request.getParameter("cellNo");
		String email = request.getParameter("userEmail");
		String userName = request.getParameter("userName");
		String password = request.getParameter("password");
		String conPass = request.getParameter("conPassword");
		int phoneNum = 0;
		int cellNum = 0;

		// ===== SAVE VALUES WHEN REFRESHED =====
		request.setAttribute("lastName", lastName);
		request.setAttribute("firstName", firstName);
		request.setAttribute("birthDate", birthDate);
		request.setAttribute("address", address);
		request.setAttribute("phoneNo", phoneNo);
		request.setAttribute("cellNo", cellNo);
		request.setAttribute("userEmail", email);
		request.setAttribute("userName", userName);
		request.setAttribute("password", password);
		request.setAttribute("conPassword", conPass);

		if (reCaptchaResponse.isValid()) {
			try {
				phoneNum = Integer.parseInt(phoneNo);
				cellNum = Integer.parseInt(cellNo);
			} catch (NumberFormatException e) {
				request.setAttribute("cellNo", "");
				request.setAttribute("phoneNo", "");
				request.setAttribute("errorMessage",
						"* Invalid contact no. Please try again");
				request.getRequestDispatcher("/signup.jsp").forward(request,
						response);
			}

			// ===== CATCH FOR ERRORS UPON VALIDATION =====
			try {
				validate(lastName, firstName, birthDate, phoneNum, cellNum,
						email, userName, password, conPass, connection,
						sqlQuery);
				// Insert user's data to db
				insertToDb(connection, sqlQuery, lastName, firstName,
						birthDate, address, email, userName, password,
						phoneNum, cellNum);

				// ===== Send EMAIL =====
				String link = generateLink(userName);
				String message = "Good day "
						+ firstName
						+ "!  You have successfully signed up in LottoGo with the username of "
						+ userName
						+ ". Please click this link to verify your acconut: ";
				Mailer mail = new Mailer();
				mail.initialize();
				mail.sendMessage(email, "LottoGo Signup Confirmation", message, link);

				// Redirect to signupConfirmation.jsp
				response.sendRedirect("signupConfirmation.jsp");
			} catch (ParseException pe) {
				request.setAttribute("errorMessage",
						"* Something went wrong. Please try again.");
				request.getRequestDispatcher("/signup.jsp").forward(request,
						response);
			} catch (EmptyFieldsException efe) {
				request.setAttribute("errorMessage", "* " + efe.getMessage());
				request.getRequestDispatcher("/signup.jsp").forward(request,
						response);
			} catch (NumberFormatException nfe) {
				request.setAttribute("cellNo", "");
				request.setAttribute("phoneNo", "");
				request.setAttribute("errorMessage",
						"* Invalid contact no. Please try again");
				request.getRequestDispatcher("/signup.jsp").forward(request,
						response);
			} catch (AgeRestrictionException are) {
				request.setAttribute("errorMessage", "* " + are.getMessage());
				request.getRequestDispatcher("/signup.jsp").forward(request,
						response);
			} catch (ExistingUserException eue) {
				request.setAttribute("userName", "");
				request.setAttribute("errorMessage", "* " + eue.getMessage());
				request.getRequestDispatcher("/signup.jsp").forward(request,
						response);
			} catch (ImpersistentPasswordException ipe) {
				request.setAttribute("password", "");
				request.setAttribute("conPassword", "");
				request.setAttribute("errorMessage", "* " + ipe.getMessage());
				request.getRequestDispatcher("/signup.jsp").forward(request,
						response);
			} catch (PasswordNotMatchException pnme) {
				request.setAttribute("password", "");
				request.setAttribute("conPassword", "");
				request.setAttribute("errorMessage", "* " + pnme.getMessage());
				request.getRequestDispatcher("/signup.jsp").forward(request,
						response);
			} catch (SQLException e) {
				request.setAttribute("errorMessage",
						"* Something went wrong. Please try again.");
				request.getRequestDispatcher("/signup.jsp").forward(request,
						response);
			} 
		} else {
			request.setAttribute("errorMessage",
					"* CAPTCHA answer not valid. Please try again.");
			request.getRequestDispatcher("/signup.jsp").forward(request,
					response);
		}

		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private String generateLink(String userName){
		String uid="";
		try {
			uid = AESencrp.encrypt(userName);
		} catch (Exception e) {}
		
		String link = "http://localhost:8080/lottogo/EmailConfirmation?uid=" + uid;
		
		return link;
	}
	
	private void insertToDb(Connection conn, SqlQuery sqlQuery,
			String lastName, String firstName, String birthDate,
			String address, String email, String userName, String password,
			int phoneNum, int cellNum) {
		User newUser = new User();
		newUser.setLastName(lastName);
		newUser.setFirstName(firstName);
		newUser.setBirthDate(birthDate);
		newUser.setAddress(address);
		newUser.setPhoneNumber(phoneNum);
		newUser.setCellNumber(cellNum);
		newUser.setEmail(email);
		newUser.setUsername(userName);
		newUser.setPassword(password);

		sqlQuery.insertNewUser(conn, newUser.getUsername(),
				newUser.getLastName(), newUser.getFirstName(),
				newUser.getBirthDate(), newUser.getAddress(),
				Integer.toString(newUser.getPhoneNumber()),
				Integer.toString(newUser.getCellNumber()), newUser.getEmail(),
				newUser.getPassword(), newUser.getMembership());
	}

	private void validate(String lastName, String firstName, String birthDate,
			int phoneNo, int cellNo, String email, String userName,
			String password, String conPass, Connection conn, SqlQuery sqlQuery)
			throws ParseException, EmptyFieldsException,
			AgeRestrictionException, PasswordNotMatchException,
			ExistingUserException, SQLException, ImpersistentPasswordException {

		int age = getAge(birthDate);

		if (lastName.matches("\\s") || firstName.matches("\\s")
				|| birthDate.matches("\\s") || email.matches("\\s")
				|| userName.matches("\\s") || password.matches("\\s")
				|| password.matches("\\s")) {
			throw new EmptyFieldsException();
		} else if (age < 18) {
			throw new AgeRestrictionException();
		} else if (phoneNo <= 0 || cellNo <= 0) {
			throw new NumberFormatException("Invalid input. Please try again");
		} else if (hasUsername(sqlQuery, conn, userName)) {
			throw new ExistingUserException();
		} else if(password.length() < 8){
			throw new ImpersistentPasswordException();
		}else if (!password.equals(conPass)) {
			throw new PasswordNotMatchException();
		}

	}

	// ===== CHECKS IF USERNAME HAS A MATCH IN DB =====
	private static boolean hasUsername(SqlQuery sqlQuery,
			Connection connection, String username) throws SQLException {
		String condition = " = ";
		ResultSet userRs = sqlQuery.rsSelectCondition(connection, "username",
				"user", condition, username, "username");
		String seenUser = "";
		while (userRs.next()) {
			seenUser = userRs.getString(1);
		}

		if (seenUser.equals("")) {
			return false;
		} else {
			return true;
		}
	}

	// ===== GET AGE OF USER ==========================
	public static int getAge(String birthDate) throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date currentDate = new Date();
		Date birthday = dateFormat.parse(birthDate);
		int age = getYears(birthday, currentDate);
		return age;
	}

	// ===== GET YEARS BETWEEN 2 DATES ================
	public static int getYears(Date birthday, Date currentDate) {
		int age = 0;

		Calendar startCalendar = new GregorianCalendar();
		startCalendar.setTime(birthday);
		Calendar endCalendar = new GregorianCalendar();
		endCalendar.setTime(currentDate);

		int diffYear = endCalendar.get(Calendar.YEAR)
				- startCalendar.get(Calendar.YEAR);
		int diffMonth = endCalendar.get(Calendar.MONTH)
				- startCalendar.get(Calendar.MONTH);
		int diffDay = endCalendar.get(Calendar.DATE)
				- startCalendar.get(Calendar.DATE);

		if (diffYear == 18) {
			if (diffMonth == 0) {
				if (diffDay >= 0) {
					age = diffYear;
				} else {
					age = diffYear - 1;
				}
			} else if (diffMonth > 0) {
				age = diffYear;
			} else {
				age = diffYear - 1;
			}
		} else {
			age = diffYear;
		}

		return age;
	}
}
