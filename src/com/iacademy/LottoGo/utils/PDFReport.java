package com.iacademy.LottoGo.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.iacademy.LottoGo.database.DBConnection;
import com.iacademy.LottoGo.database.SqlQuery;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

@WebServlet("/PDFReport")
public class PDFReport extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text.html");	
			 getContents();
			
			 servePDF(request, response);
		
	}
	
	private void servePDF(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String nameOfPDF = "LottoGo.pdf";
		String contextPath = getServletContext().getRealPath(File.separator);
		File pdfFile = new File(contextPath + nameOfPDF);
		System.out.println(pdfFile);

		response.setContentType("application/pdf");
		response.addHeader("Content-Disposition", "attachment; filename="
				+ nameOfPDF);
		response.setContentLength((int) pdfFile.length());

		FileInputStream inputStream = new FileInputStream(pdfFile);
		OutputStream outputStream = response.getOutputStream();

		int bytes;
		while ((bytes = inputStream.read()) != -1) {
			outputStream.write(bytes);
		}
	
	}
	
	private void getContents () {
		Connection connection = null;
		connection = DBConnection.getConnection();
		SqlQuery sqlQuery = new SqlQuery();
		ResultSet rsSales ;
		ResultSet rsWinningNumber;
		ResultSet rsWinners;
		
		//  Get no. of total draws
		ResultSet rsWinningNo = sqlQuery.rsSelect(connection, "COUNT(draw_id)", "winning_number", "draw_id");

		int	totalDraws = 0;
		try {
			
			while (rsWinningNo.next()) {
				totalDraws = rsWinningNo.getInt(1);
				//==System.out.print("\n" + totalSales);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		String winningNumbers[] = new String [totalDraws+1];
		String winners [] = new String [totalDraws+1];
		String totalSales[] = new String [totalDraws+1];
	
		for (int draw = 1; draw <=totalDraws; draw++) {
		System.out.println("\ndraw no. = " + draw);
			
			// GET total sales
			 rsSales = sqlQuery.rsSelectCondition(connection, "COUNT(bet_id)", "bet", " draw_id = "+ draw);
			 try {
				while (rsSales.next()) {
						totalSales[draw] = "Total sales: PHP " + rsSales.getInt(1)*10 + ".00";
						
					}
			} catch (SQLException e) {
			}
			 
			 // Get winning number 
			 rsWinningNumber = sqlQuery.rsSelectCondition(connection, "first_number, second_number, third_number, fourth_number, fifth_number, sixth_number",
					 "winning_number", "draw_id = "+ draw);
				 try {
					while (rsWinningNumber.next()) {
							
							winningNumbers[draw]= "Draw ID: " + draw + "\n" +
									"Winning Combination:" +rsWinningNumber.getString(1)+ " - " + rsWinningNumber.getString(2)+ " - " +
									rsWinningNumber.getString(3) + " - " +rsWinningNumber.getString(4)+ " - " + rsWinningNumber.getString(5)+  " - " +
									rsWinningNumber.getString(6);
							
						}
				} catch (SQLException e) {
				}
				 
			 // get winners
			rsWinners = sqlQuery.rsInnerJoin(connection, " *", "user", "winners", "username", "draw_id = "+draw);
			
			try {
				while (rsWinners.next()) {

					winners [draw] = ("\n") +rsWinners.getString("first_name") + " " + rsWinners.getString("last_name") 
							+ " (" + rsWinners.getString("username")+ ")" 
							+ "\n\tBet: " + rsWinners.getString("first_number")+ " - " + rsWinners.getString("second_number")+ " - " 
							+ rsWinners.getString("third_number") + " - " + rsWinners.getString("fourth_number")+ " - " 
							+ rsWinners.getString("fifth_number")+  " - " +	rsWinners.getString("sixth_number") 
							+ "\n\tPrize Won : " + rsWinners.getString("amount_won");
				
					System.out.println("Winner: " + winners[draw]);
				}
			} catch (SQLException e) {
			}
		}
		
	//  Get no. of total no of members
				ResultSet rsUsers = sqlQuery.rsSelect(connection, "COUNT(username)", "user", "username");

				int	totalUsers = 0;
				try {
					
					while (rsUsers.next()) {
						totalUsers = totalUsers + rsUsers.getInt(1);
						
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			
		System.out.println("users : " + totalUsers);
		String members[] = new String [totalUsers-1];
		
		ResultSet rs = sqlQuery.rsSelectCondition(connection, "*", "user", " membership != 'admin'");
		
		int user = 0;
			try {
				while (rs.next()) {
						
				members[user] = "User: " + 
						rs.getString("first_name") + " " +
						rs.getString("last_name") + " (" +
						rs.getString("username") + " )"+ "\n"+
						"Address: " + rs.getString("address") +  "\n"+
						"Email: " + rs.getString("email")+  "\n"+
						"Contact No.: " + rs.getString("phone_number") + " / " +
						rs.getString("cell_number");
				System.out.println(members[user]);
				
				user++;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			//System.out.println(members[user]);
			
		//System.out.println("heeeeeeeere" + members[2]);
		createPDFReport(winners, winningNumbers, totalSales, totalDraws, members);
		
	}
	
	public void createPDFReport (String [] winners, String [] winningNumbers, String [] totalSales, int totalDraws, String [] members){
			
			
			try {
				OutputStream file =
						new FileOutputStream(new File ("C:/Enterprise Java/eclipse_ws_jee/LottoGo/web/LottoGo.pdf"));
				
				Document document = new Document();
				PdfWriter.getInstance(document, file);
				document.open ();
				
				Image image = Image.getInstance("C:/Enterprise Java/eclipse_ws_jee/LottoGo/web/images/Logo.png");
				 image.setAlignment(Element.ALIGN_CENTER);
		        document.add(image);
		      
		        Paragraph header = new Paragraph();	       
		       
		        header.add("LottoGo � .\n");
		        header.add("6/45 Online Lotto \n");
				header.add("Tel. No. # 551-4301 to 05\n");
				header.add("Vat-Reg Tin: 240-798-564-000\n");
				header.add("Accreditation:  # 116-001668824-000038\n\n");
				
				header.setAlignment(Element.ALIGN_CENTER);
				document.add(header);
				
				Paragraph body = new Paragraph();
				body.add("      ===================================================================\n");
				
				for (int ctr = 1; ctr <= totalDraws; ctr++) {
					
					body.add("\n" + winningNumbers[ctr] + "\n");
					body.add(totalSales[ctr] + "\n" );
					
					if ((winners[ctr] != null)) {
						body.add(winners[ctr] + "\n");
					} else {
						body.add ("\nNo Winners.\n");
					}
					
					body.add("-----------------------------------------------");
					
				}
				
				body.add("\n\nMembers:\n\n");

				
				for (int count = 0; count < members.length; count++) {
					System.out.println(count);
					body.add("\n" + members[count] + "\n");
					System.out.println(members[count] + "\n");
					
				}
				body.add("\n\n\n     ===================================================================\n\n");
				
				body.setAlignment(Element.ALIGN_LEFT);
				document.add(body);
				
				
				Paragraph footer = new Paragraph();
				footer.add("THIS SERVES AS YOUR OFFICIAL SALES REPORT\n");
				footer.add("Per DTI-NCR Permit No. 1010,\n Series of 2014.\n");
				
				footer.setAlignment(Element.ALIGN_CENTER);
				document.add(footer);
				
				
				document.addAuthor("Team HAHA");
				document.addTitle("LottoGo");
				
				System.out.println("\n\nReport created.\n");
				document.close();
				file.close();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		
}
