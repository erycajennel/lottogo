package com.iacademy.LottoGo.utils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.iacademy.LottoGo.beans.Bet;
import com.iacademy.LottoGo.database.DBConnection;
import com.iacademy.LottoGo.database.SqlQuery;
import com.iacademy.LottoGo.exceptions.ExpiredCreditCardException;
import com.iacademy.LottoGo.exceptions.InvalidCreditCardException;

@WebServlet("/ProcessPayment")
public class ProcessPayment extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// ===== CONNECT DB =====
		Connection connection = null;
		SqlQuery sqlQuery = new SqlQuery();

		connection = DBConnection.getConnection();

		String cardNumber = request.getParameter("cardNumber");
		String expirationDate = request.getParameter("expirationDate");

		HttpSession userSession = request.getSession(false);
		Bet userBet = (Bet) userSession.getAttribute("currentBet");
		String username = (String) userSession.getAttribute("userName");
		
		if(request.getParameter("payButton") != null){
			try {
				// VALIDATE CARD
				validateCard(cardNumber, expirationDate);

				// INSERT BET TO DB
				try {
					insertBetToDb(connection, username, sqlQuery, userBet);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				//REMOVE BET ATTRIBUTE
				userSession.removeAttribute("currentBet");
				userSession.setAttribute("payment", "paid");
				
				response.sendRedirect("paymentConfirmation.jsp");
			} catch (InvalidCreditCardException icce) {
				request.setAttribute("errorMessage", "* " + icce.getMessage());
				request.getRequestDispatcher("/payment.jsp").forward(
						request, response);
			} catch (ParseException pe) {
				request.setAttribute("errorMessage", "* " + pe.getMessage());
				request.getRequestDispatcher("/payment.jsp").forward(
						request, response);
			} catch (ExpiredCreditCardException ecce) {
				request.setAttribute("errorMessage", "* " + ecce.getMessage());
				request.getRequestDispatcher("/payment.jsp").forward(
						request, response);
			}
		}else{
			// INSERT BET TO DB
			try {
				insertBetToDb(connection, username, sqlQuery, userBet);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			//REMOVE BET ATTRIBUTE
			userSession.removeAttribute("currentBet");
			userSession.setAttribute("payment", "paid");
			
			response.sendRedirect("paymentConfirmation.jsp");
		}
	}

	// ===== Insert/Register Bet to DB =====
	private void insertBetToDb(Connection connection, String userName,
			SqlQuery sqlQuery, Bet userBet) throws SQLException {
		int drawId = 0;
		ResultSet rsWinNum = sqlQuery.rsSelectLastEntry(connection,
				"winning_number", "draw_id");
		while (rsWinNum.next()) {
			drawId = rsWinNum.getInt(1);
		}

		sqlQuery.insertNewBet(connection, userName, userBet.getFirstNum(),
				userBet.getSecondNum(), userBet.getThirdNum(), userBet.getFourthNum(),
				userBet.getFifthNum(), userBet.getSixthNum(), userBet.getStatus(), drawId);
	}

	// ===== VALIDATE CARD =====
	private void validateCard(String cardNumber, String expirationDate)
			throws InvalidCreditCardException, ParseException,
			ExpiredCreditCardException {
		if (!checkCardValidity(cardNumber)) {
			throw new InvalidCreditCardException();
		} else if (isExpired(expirationDate)) {
			throw new ExpiredCreditCardException();
		}
	}

	// ===== Luhn Algorithm =====
	private boolean checkCardValidity(String cardNumber) {
		int oddIndices = 0;
		int evenIndices = 0;
		String reversedNum = new StringBuffer(cardNumber).reverse().toString();
		for (int ctr = 0; ctr < reversedNum.length(); ctr++) {
			int currentVal = Character.getNumericValue(reversedNum.charAt(ctr));

			if (ctr % 2 == 0) {
				evenIndices += currentVal;
			} else {
				int oddIndex = currentVal * 2;

				if (oddIndex > 9) {
					String tensDig = String.valueOf(oddIndex);
					oddIndices += Character.getNumericValue(tensDig.charAt(0))
							+ Character.getNumericValue(tensDig.charAt(1));
				} else {
					oddIndices += oddIndex;
				}
			}
		}
		return ((evenIndices + oddIndices) % 10 == 0);
	}

	// ===== Checks if CC is expired =====
	private boolean isExpired(String expirationDate) throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date currentDate = new Date();
		Date expDate = dateFormat.parse(expirationDate);

		Calendar startCalendar = new GregorianCalendar();
		startCalendar.setTime(expDate);
		Calendar endCalendar = new GregorianCalendar();
		endCalendar.setTime(currentDate);

		if (startCalendar.get(Calendar.MONTH) <= endCalendar
				.get(Calendar.MONTH)
				&& startCalendar.get(Calendar.YEAR) <= endCalendar
						.get(Calendar.YEAR)) {
			return true;
		} else if (startCalendar.get(Calendar.YEAR) < endCalendar
				.get(Calendar.YEAR)) {
			return true;
		} else {
			return false;
		}

	}
}
