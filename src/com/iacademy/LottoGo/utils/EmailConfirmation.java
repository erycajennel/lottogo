package com.iacademy.LottoGo.utils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.iacademy.LottoGo.database.DBConnection;
import com.iacademy.LottoGo.database.SqlQuery;

@WebServlet("/EmailConfirmation")
public class EmailConfirmation extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processEmail(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processEmail(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void processEmail(HttpServletRequest request, HttpServletResponse response) throws Exception{
		Connection connection = null;
		SqlQuery sqlQuery = new SqlQuery();
		
		connection = DBConnection.getConnection();	
		
		String userId = AESencrp.decrypt(request.getParameter("uid"));
		
		if(hasAccount(sqlQuery, connection, userId)){
			String membership = getMembership(sqlQuery, connection, userId);
			
			// == CREATE SESSION ==
			
			HttpSession userSession = request.getSession();
			userSession.setAttribute("membership", membership);
			userSession.setAttribute("userName", userId);
			userSession.setMaxInactiveInterval(10* 60);
			
			response.sendRedirect("home.jsp");
		}else{
			response.sendRedirect("error.jsp");
		}
		
		connection.close();
	}
	
	private static boolean hasAccount(SqlQuery sqlQuery, Connection connection, String username) throws SQLException{
		ResultSet userRs = sqlQuery.rsSelect(connection, "user WHERE username = '"+ username + "' ", "username");
		String seenUser = "";
		while(userRs.next()){
			seenUser = userRs.getString(1);
		}
		
		if(seenUser.equals("")){
			return false;
		}else{
			return true;
		}
	}
	// == GETS THE TYPE OF MEMBERSHIP ==
	private static String getMembership(SqlQuery sqlQuery, Connection connection, String username) throws SQLException{
		ResultSet membershipType = sqlQuery.rsSelect(connection, "user WHERE username = '"+ username + "' ", "username");
		String membership = "";
		while(membershipType.next()){
			membership = membershipType.getString(10);
		}
		return membership;
	}

}
