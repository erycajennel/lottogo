package com.iacademy.LottoGo.utils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.iacademy.LottoGo.database.DBConnection;
import com.iacademy.LottoGo.database.SqlQuery;

@WebServlet("/ProcessDraw")
public class ProcessDraw extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
	Connection connection = null;
	SqlQuery sqlQuery = new SqlQuery();
	int jackpot = 0;
	

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text.html");
		
		HttpSession adminSession = request.getSession();
		
		System.out.println(request.getAttribute("drawStatus"));
		if (adminSession.getAttribute("drawStatus") == null) {
			
			adminSession.setAttribute("drawStatus", true);
			System.out.println(adminSession.getAttribute("drawStatus"));
			connection = DBConnection.getConnection();
			
			int winningNumber[] = randomize(request, response);

			// to get jackpot prize

			ResultSet rsBets = sqlQuery.rsSelect(connection, "COUNT(bet_id)", "bet", "bet_id");
			try {
				while (rsBets.next()) {
					jackpot = 9000000  + rsBets.getInt(1)*10 ;
				}
			} catch (SQLException e) {}
						
			// insert winning numbers
			sqlQuery.insertQuery(connection, winningNumber[0],  winningNumber[1],  winningNumber[2],  winningNumber[3],
					 winningNumber[4],  winningNumber[5], jackpot) ;
			
			compareToBets(winningNumber);
			
		} else {
			request.getRequestDispatcher("draw.jsp").forward(request, response);
		}
			
	}
	
	private int [] randomize(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		
		//drawSession = request.getSession();
		HttpSession adminSession = request.getSession();
		
		int[] randNum = generateRandomNumber();

		adminSession.setAttribute("firstNumber", randNum[0]);
		adminSession.setAttribute("secondNumber", randNum[1]);
		adminSession.setAttribute("thirdNumber", randNum[2]);
		adminSession.setAttribute("fourthNumber", randNum[3]);
		adminSession.setAttribute("fifthNumber", randNum[4]);
		adminSession.setAttribute("sixthNumber", randNum[5]);
		adminSession.setAttribute("disable", "disabled");

		request.getRequestDispatcher("/draw.jsp").forward(request, response);
		
		return randNum;
		
	}

	// ===== RANDOMIZE ENTRIES (FOR LUCKY PICK) =====
	public int[] generateRandomNumber() {
		int[] numbersBet = new int[6];
		Random random = new Random();

		boolean isDuplicate = true;

		while (isDuplicate) {
			for (int ctr = 0; ctr < numbersBet.length; ctr++) {
				numbersBet[ctr] = 1 + random.nextInt(45);
			}
			isDuplicate = checkForDuplicate(numbersBet);
		}
		return numbersBet;
	}

	// ===== CHECKS IF THERE ARE ANY DUPLICATES IN THE ARRAY =====
	public boolean checkForDuplicate(int[] numbersBet) {

		boolean isDuplicate = false;

		for (int j = 0; j < numbersBet.length; j++) {

			for (int k = j + 1; k < numbersBet.length; k++) {

				if (numbersBet[k] == numbersBet[j]) {
					isDuplicate = true;
					return isDuplicate;
				} else {
					isDuplicate = false;
				}
			}
		}
		return isDuplicate;
	}
	
	public void compareToBets (int [] winningNumbers) {
		
		@SuppressWarnings("unused")
		ResultSet rsUpdate;
		connection = DBConnection.getConnection();
		int amountWon = 0;
		
		// Get last draw ID
		
		int last_draw_id= 0; ;
		ResultSet rsLastDrawID = sqlQuery.rsSelectLastColumn(connection,
				"winning_number", "draw_id", "draw_id");
			try {
				while (rsLastDrawID.next()) {
					last_draw_id = rsLastDrawID.getInt(1);
				}
			} catch (SQLException e) {
			}
			
		// Get current bettors
		ResultSet rsBettors = sqlQuery.rsSelectCondition(connection,
				"*", "bet", " draw_id ="+last_draw_id);
		int betNumbers[] = new int [6];
			try {
				while (rsBettors.next()) {
					
					String betId = rsBettors.getString("bet_id");
					String username = rsBettors.getString("username");
					int drawId = rsBettors.getInt("draw_id");
					betNumbers[0] = rsBettors.getInt("first_number");
					betNumbers[1] = rsBettors.getInt("second_number");
					betNumbers[2] = rsBettors.getInt("third_number");
					betNumbers[3] = rsBettors.getInt("fourth_number");
					betNumbers[4] = rsBettors.getInt("fifth_number");
					betNumbers[5] = rsBettors.getInt("sixth_number");
					
					String status = matchNumbers(betNumbers, winningNumbers);
					
					// UPDATE STATUS OF THE BETTORS
					sqlQuery.rsUpdate(connection, "status", status, "bet", "bet_id = "+betId);
					if (!status.equals("lose")) {
						
					switch(status) {
						case "4th" : amountWon = 40 ;
							break;
						case "3rd" : amountWon = 600 ;
							break;
						case "2nd" : amountWon = 23000 ;
							break;
						case "1st" : amountWon = jackpot;
							break;
					}
						sqlQuery.insertQuery(connection, username, betNumbers[0], betNumbers[1], betNumbers[2], betNumbers[3], 
								betNumbers[4], betNumbers[5], status, amountWon, drawId);
						
					}
					
				}
			} catch (SQLException e) {
			}
		
			
	}
	
	public String matchNumbers (int [] betNumbers, int [] winningNumbers ) {
			
			String status = null;
			int matched = 0;
			
			for (int ctr = 0; ctr < winningNumbers.length; ctr++) {
				for (int ctr2 = 0; ctr2 < winningNumbers.length; ctr2++) {
					System.out.println(betNumbers[ctr] +" vs." + winningNumbers[ctr2] );
					if (betNumbers[ctr] == winningNumbers[ctr2]) {
						matched ++;
						System.out.println(matched);
					}
				}
			}
			
			switch (matched) {
				case 3 : status = "4th";
					break;
				case 4 : status = "3rd";
					break;
				case 5 : status = "2nd";
					break;
				case 6 : status = "1st";
					break;
				default : status = "lose";
					break;
				
			}
			System.out.println(status);
			return status;
			
		}

}
