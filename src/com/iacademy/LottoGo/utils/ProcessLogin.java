package com.iacademy.LottoGo.utils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.tanesha.recaptcha.ReCaptchaImpl;
import net.tanesha.recaptcha.ReCaptchaResponse;

import com.iacademy.LottoGo.database.DBConnection;
import com.iacademy.LottoGo.database.SqlQuery;
import com.iacademy.LottoGo.exceptions.UserPassNotMatchException;

@WebServlet("/ProcessLogin")
public class ProcessLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		
		// ===== CONNECT DB =====
		Connection connection = null;
		SqlQuery sqlQuery = new SqlQuery();
		
		connection = DBConnection.getConnection();	
		
		// ===== SETUP RECAPTCHA VALIDATION =====
		String remoteAddr = request.getRemoteAddr();
		ReCaptchaImpl reCaptcha = new ReCaptchaImpl();
		reCaptcha.setPrivateKey("6LdlHOsSAAAAACe2WYaGCjU2sc95EZqCI9wLcLXY");

		String challenge = request.getParameter("recaptcha_challenge_field");
		String uresponse = request.getParameter("recaptcha_response_field");
		ReCaptchaResponse reCaptchaResponse = reCaptcha.checkAnswer(
				remoteAddr, challenge, uresponse);

		String username = request.getParameter("username");
		String password = request.getParameter("userPassword");
		String remember = request.getParameter("remember");
		
		// ===== CHECK VALIDITY =====
		if (reCaptchaResponse.isValid()) {
			try {
				validate(connection, sqlQuery, username, password);
				
				String membership = getMembership(sqlQuery, connection, username);
				
				// == CREATE SESSION. IF remember.equals(true), CREATE COOKIE;
				if(remember != null){
					String usernameEnc = username;
					String memberEnc = membership;
					
					try {
						usernameEnc = AESencrp.encrypt(username);
						memberEnc = AESencrp.encrypt(membership);
					} catch (Exception e) {
					}					
					
					Cookie userCookie = new Cookie("lottoGoUser", usernameEnc);
					//make the cookie persistent and set its expiration to 20 years
					userCookie.setMaxAge(20* 12* 4* 7* 24* 60* 60);
					response.addCookie(userCookie);
					
					Cookie memberCookie = new Cookie("lottoGoMember", memberEnc);
					//make the cookie persistent and set its expiration to 20 years
					memberCookie.setMaxAge(20* 12* 4* 7* 24* 60* 60);
					response.addCookie(memberCookie);
				}
				
				HttpSession userSession = request.getSession();
				userSession.setAttribute("membership", membership);
				userSession.setAttribute("userName", username);
				userSession.setMaxInactiveInterval(10* 60);
				
				response.sendRedirect("home.jsp");
				
			} catch (SQLException e) {
				request.setAttribute("toggle", "#login");
				request.setAttribute("errorMessage", "* Something went wrong. Please try again.");
				request.getRequestDispatcher(".").forward(request, response);
			} catch (UserPassNotMatchException e) {
				request.setAttribute("toggle", "#login");
				request.setAttribute("errorMessage", e.getMessage());
				request.getRequestDispatcher(".").forward(request, response);
			}
		} else {
			request.setAttribute("toggle", "#login");
			request.setAttribute("errorMessage", "* CAPTCHA answer not valid. Please try again.");
			request.getRequestDispatcher(".").forward(request, response);
		}
	
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	// ===== VALIDATES USERNAME AND PASS =====
	private void validate(Connection conn, SqlQuery sqlQuery, String username,
			String password) throws SQLException, UserPassNotMatchException {
		
		if(!hasAccount(sqlQuery, conn, username, password)){
			throw new UserPassNotMatchException();
		} 		
	}
	
	// ===== CHECKS IF USER HAS AN ACCOUNT =====
	private static boolean hasAccount(SqlQuery sqlQuery, Connection connection, String username, String password) throws SQLException{
		String condition = " = ";
		ResultSet userRs = sqlQuery.rsSelectAccount(connection, "username",
				"password", "user", condition, condition, username, password,
				"username");
		String seenUser = "";
		while(userRs.next()){
			seenUser = userRs.getString(1);
		}
		
		if(seenUser.equals("")){
			return false;
		}else{
			return true;
		}
	}

	private static String getMembership(SqlQuery sqlQuery, Connection connection, String username) throws SQLException{
		ResultSet membershipType = sqlQuery.rsSelect(connection, "user WHERE username = '"+ username + "' ", "username");
		String membership = "";
		while(membershipType.next()){
			membership = membershipType.getString(10);
		}
		
		return membership;
	}
}
