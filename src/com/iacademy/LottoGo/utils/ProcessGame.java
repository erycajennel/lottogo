package com.iacademy.LottoGo.utils;

import java.io.IOException;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.iacademy.LottoGo.beans.Bet;
import com.iacademy.LottoGo.exceptions.EmptyFieldsException;
import com.iacademy.LottoGo.exceptions.EntryOutOfBoundsException;
import com.iacademy.LottoGo.exceptions.RepeatingNumbersException;

@WebServlet("/processGame")
public class ProcessGame extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		if(request.getParameter("luckyPick") != null){
			randomize(request, response);
		}else if (request.getParameter("submit") != null){
			try {
				validateEntries(request, response);
				addBetToSession(request, response);
				response.sendRedirect("payment.jsp");
			} catch (EmptyFieldsException efe) {
				request.setAttribute("errorMessage", "* " + efe.getMessage());
				request.getRequestDispatcher("/play.jsp").forward(request,
						response);
			} catch(NumberFormatException nfe){
				request.setAttribute("errorMessage", "* Invalid input. Please Check for blank fields, or non-integer entries");
				request.getRequestDispatcher("/play.jsp").forward(request,
						response);
			} catch(EntryOutOfBoundsException eobe){
				request.setAttribute("errorMessage", "* " + eobe.getMessage());
				request.getRequestDispatcher("/play.jsp").forward(request,
						response);
			} catch(RepeatingNumbersException rne){
				request.setAttribute("errorMessage", "* " + rne.getMessage());
				request.getRequestDispatcher("/play.jsp").forward(request,
						response);
			}
		}else{
			response.sendRedirect("play.jsp");
		}
	}
	
	// ===== SAVE VALUES TO BET OBJECT =====
	private void addBetToSession(HttpServletRequest request,
			HttpServletResponse response){
		HttpSession userSession = request.getSession();
		
		Bet newBet = new Bet();
		newBet.setUserName((String)userSession.getAttribute("userName"));
		newBet.setFirstNum(Integer.parseInt(request.getParameter("1stNum")));
		newBet.setSecondNum(Integer.parseInt(request.getParameter("2ndNum")));
		newBet.setThirdNum(Integer.parseInt(request.getParameter("3rdNum")));
		newBet.setFourthNum(Integer.parseInt(request.getParameter("4thNum")));
		newBet.setFifthNum(Integer.parseInt(request.getParameter("5thNum")));
		newBet.setSixthNum(Integer.parseInt(request.getParameter("6thNum")));
		newBet.setStatus("pending");
		
		userSession.setAttribute("currentBet", newBet);
	}

	// ===== VALIDATES THE ENTERED VALUES =====
	private void validateEntries(HttpServletRequest request,
			HttpServletResponse response) throws EmptyFieldsException, EntryOutOfBoundsException, RepeatingNumbersException {
		int firstNum = Integer.parseInt(request.getParameter("1stNum"));
		int secondNum = Integer.parseInt(request.getParameter("2ndNum"));
		int thirdNum = Integer.parseInt(request.getParameter("3rdNum"));
		int fourthNum = Integer.parseInt(request.getParameter("4thNum"));
		int fifthNum = Integer.parseInt(request.getParameter("5thNum"));
		int sixthNum = Integer.parseInt(request.getParameter("6thNum"));
		int[] entries = {firstNum, secondNum, thirdNum, fourthNum, fifthNum, sixthNum};

		if(!inBound(entries)){
			throw new EntryOutOfBoundsException();
		}else if(checkForDuplicate(entries)){
			throw new RepeatingNumbersException();
		}
	}
	
	// ===== CHECK IF THE ENTRIES ARE <= 0 OR > 45 =====
	private boolean inBound(int[] entries){
		for(int entry: entries){
			if(entry<=0 || entry > 45){
				return false;
			}
		}
		return true;
	}

	// ===== LUCKY PICK FUNCTION =====
	private void randomize(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		int[] randNum = generateRandomNumber();

		request.setAttribute("firstNumber", randNum[0]);
		request.setAttribute("secondNumber", randNum[1]);
		request.setAttribute("thirdNumber", randNum[2]);
		request.setAttribute("fourthNumber", randNum[3]);
		request.setAttribute("fifthNumber", randNum[4]);
		request.setAttribute("sixthNumber", randNum[5]);
		request.setAttribute("focus", "#mainContainer");

		request.getRequestDispatcher("/play.jsp").forward(request, response);
	}

	// ===== RANDOMIZE ENTRIES (FOR LUCKY PICK) =====
	public static int[] generateRandomNumber() {
		int[] numbersBet = new int[6];
		Random random = new Random();

		boolean isDuplicate = true;

		while (isDuplicate) {
			for (int ctr = 0; ctr < numbersBet.length; ctr++) {
				numbersBet[ctr] = 1 + random.nextInt(45);
			}
			isDuplicate = checkForDuplicate(numbersBet);
		}
		return numbersBet;
	}

	// ===== CHECKS IF THERE ARE ANY DUPLICATES IN THE ARRAY =====
	public static boolean checkForDuplicate(int[] numbersBet) {

		boolean isDuplicate = false;

		for (int j = 0; j < numbersBet.length; j++) {

			for (int k = j + 1; k < numbersBet.length; k++) {

				if (numbersBet[k] == numbersBet[j]) {
					isDuplicate = true;
					return isDuplicate;
				} else {
					isDuplicate = false;
				}
			}
		}
		return isDuplicate;
	}

}
