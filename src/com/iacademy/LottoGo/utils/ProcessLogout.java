package com.iacademy.LottoGo.utils;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/logout")
public class ProcessLogout extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		
		// retrieve all cookies from the user web browser
		Cookie[] allCookies = request.getCookies();
		Cookie userCookie = null;
		Cookie memCookie = null;
		
		// iterate all the cookie array and look for the target cookie created by the processLogin servlet
		if(allCookies != null){
			for(Cookie cookie : allCookies){
				if(cookie.getName().equals("lottoGoUser")){
					userCookie = cookie;
					userCookie.setMaxAge(0);
					response.addCookie(userCookie);
				}if (cookie.getName().equals("lottoGoMember")){
					memCookie = cookie;
					memCookie.setMaxAge(0);
					response.addCookie(memCookie);
				}
			}					
		}
		
		HttpSession userSession = request.getSession();
		userSession.invalidate();
		response.sendRedirect("home.jsp");
	}
}
